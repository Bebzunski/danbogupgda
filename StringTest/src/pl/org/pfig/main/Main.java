package pl.org.pfig.main;

import java.util.Random;

public class Main {

	public static void main(String[] args) {
		
		int[][] testArray = {{7,1},{14,49},{2,5}};
		
		TwoDArrays m = new TwoDArrays();
		System.out.println(m.addElementsDividedBySeven(testArray));
		

		
		Matrix m1 = new Matrix();
		int[][] arrow = {{1,2,3},{4,5,6},{7,8,9}};
		m1.printMatrix(m1.unitMatrix(arrow));
		m1.printMatrix(m1.inSequence(arrow));


	
		System.exit(0);
		
		System.out.println();
		StringUtil su = new StringUtil("Daniel Bogdalski");
		su.print().pokemon().print();
		
		
		

		String myStr = " przyk�adowy ci�g znak�w  ";

		if (myStr.equals(" przyk�adowy ci�g znak�w  ")) {
			System.out.println("Ci�g znak�w taki sam");
		}

		if (myStr.equalsIgnoreCase(" PRZYK�ADOWY ci�g znaK�W  ")) {
			System.out.println("Ciagi znak�w s� takie same, bez badania ci�g�o�ci znak�w");
		}

		System.out.println("d�ugo�c myStr to: " + myStr.length());

		System.out.println(myStr.substring(14));

		String otherStr = "pawe�ek";
		System.out.println(otherStr.substring(2));
		System.out.println(otherStr.substring(2, 4));
		System.out.println(otherStr.substring(2, otherStr.length() - 1));
		System.out.println(myStr.trim());
		System.out.println("" + myStr.charAt(3) + myStr.charAt(5));

		String alphabet = "";
		for (char c = 97; c <= 122; c++) {
			alphabet += c + " ";
		}
		System.out.println(alphabet);

		System.out.println(myStr.replace("ci�g", "lancuch"));
		System.out.println(myStr.concat(otherStr));

		System.out.println(myStr);

		if (myStr.contains("k�ad")) {
			System.out.println("S�owo 'k�ad' zawiera si� w " + myStr);
		}
		if(alphabet.startsWith("a")){
			System.out.println("alfabet zaczyna si� od A");
		}
		if(alphabet.trim().endsWith("z")){
			System.out.println("konczy sie na Z");
		}
		System.out.println(myStr.indexOf("a"));
		System.out.println(myStr.lastIndexOf("a"));
		
		String simpleStr = "pawel poszedl do lasu";
		String[] arrOfStr = simpleStr.split(" ");
		for(String s : arrOfStr){
			System.out.println("\t"+s);
		}
		
		
		String wordStr = "kaktus aka";
		System.out.println(wordStr);
		char firstLetter = wordStr.charAt(0);
		System.out.println(firstLetter+wordStr.substring(1).replace(firstLetter, '_'));	
		
		
		String[] w = Main.checkWordsLength(5, "kiedy� p�jd� do lasu na grzyby");
		
		for (String str : w) {
			if (str != null) {
				System.out.println(str);
			}
		}
		
		Random r = new Random();
		System.out.println(r.nextInt()); // bez parametru moze byc liczba ujemna
		System.out.println(r.nextInt(15)); // z argumentem  zakres to [0 , 15)
		
		// wygenerowac liczbe z przedzia�u [10,25)
		int a = 10;
		int b = 25;
		System.out.println(a + r.nextInt(b-a));
		
		char c = (char) (97+r.nextInt(26));
		System.out.println(c+"");
		
		System.out.println();
		HTMLExercise newHTML = new HTMLExercise("to jest moj nowy tekst");
		newHTML.print().strong().print().p().print();
		

	}
	
	public static String[] checkWordsLength(int howLong, String sentence) {

		String[] arrOfWords1 = sentence.split(" ");
		String[] arrOfWords2 = new String[arrOfWords1.length];

		int i = 0;
		for (String s : arrOfWords1) {

			if (s.length() > howLong) {
				arrOfWords2[i++] = s;
			}
		}
		return arrOfWords2;
	}

}
