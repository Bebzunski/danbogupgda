package pl.org.pfig.main;

public class Matrix {
	
	
	public void printMatrix(int[][] arrow) {

		for (int[] row : arrow) {
			for (int number : row) {
				System.out.print(number + " ");
			}
			System.out.println();
		}
	}
	
	public int[][] unitMatrix(int[][] arrow){
		
		for(int[] row : arrow){
			for(int i=0;i<row.length;i++){
				row[i] = 1;
			}
		}
		
		return arrow;
	}
	
	public int[][] inSequence(int[][] arr){
		
		for(int[] rows : arr){
			for(int i = 0;i<rows.length ; i++){
				rows[i]=i+1;
			}
		}
		
		return arr;
	}

}
