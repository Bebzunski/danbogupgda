package pl.org.pfig.main;

public class StringUtil {

	private String str;

	public StringUtil(String str) {
		this.str = str;

	}

	public StringUtil print() {
		System.out.println(str);
		return this;
	}

	public StringUtil prepend(String arg) {
		this.str = arg + this.str;
		return this;
	}

	public StringUtil append(String arg) {

		str = str + arg;
		return this;
	}

	public StringUtil reverse() {
		String reverseWord = "";
		for (int i = str.length() - 1; i >= 0; i--) {
			reverseWord += str.charAt(i);
		}
		str = reverseWord;
		return this;
	}

	public StringUtil getFirstLetter() {

		str = "" + str.charAt(0);
		return this;
	}

	public StringUtil limit(int whereEnds) {

		str = str.substring(0, whereEnds + 1);
		return this;
	}

	public StringUtil insertAt(String arg, int n) {
		if (n >= 0 && n <= str.length()) {
			str = str.substring(0, n) + arg + str.substring(n, str.length());
		}
		return this;
	}

	public StringUtil resetText() {
		str = "";
		return this;
	}

	public StringUtil swapLetters() {

		str = str.charAt(str.length() - 1) + str.substring(1, str.length() - 1) + str.charAt(0);
		return this;
	}

	public StringUtil createSentence() {
		if (str.charAt(str.length() - 1) != '.') {
			str = ("" + str.charAt(0)).toUpperCase() + str.substring(1, str.length()) + ".";
		} else {
			str = ("" + str.charAt(0)).toUpperCase() + str.substring(1);
		}
		return this;
	}

	public StringUtil cut(int from, int to) {

		String[] arr = str.split("");
		str = "";
		for (int i = from; i <= (to - from + 1); i++) {
			str += arr[i];
		}
		return this;
	}

	public StringUtil pokemon() {

		String[] arr = str.split("");
		str="";
		for (int i = 0; i < arr.length-1; i+=2) {
			if (arr[i].equals(" ")) {
				
			} else {
				str=str+arr[i].toLowerCase()+arr[i+1].toUpperCase();
			}
		}
		return this;
	}

	public StringUtil letterSpacing() {
		String[] arr = str.split("");
		str = "";
		for(String s : arr){
		str=str+s+" ";	
		}
		str=str.trim();
		return this;
	}
}
