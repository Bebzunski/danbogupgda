package main;

public class Person {
	
	private String name;
	private String secondName;
	private int age;
	private String hair;
	private String eyes;
	private String shoes;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) throws WrongAgeException{
		
		checkAge(age);
		this.age = age;
	}

	private void checkAge(int age) throws WrongAgeException {
		if(age<=0 | age>=130){
			throw new WrongAgeException("niepoprawny wiek");
		}
	}

	public String getHair() {
		return hair;
	}

	public void setHair(String hair) {
		this.hair = hair;
	}

	public String getEyes() {
		return eyes;
	}

	public void setEyes(String eyes) {
		this.eyes = eyes;
	}

	public String getShoes() {
		return shoes;
	}

	public void setShoes(String shoes) {
		this.shoes = shoes;
	}

	public Person(String name, String secondName, int age, String hair, String eyes, String shoes) throws WrongAgeException {

		this.name = name;
		this.secondName = secondName;
		
		checkAge(age);
		this.age = age;
		
		this.hair = hair;
		this.eyes = eyes;
		this.shoes = shoes;
	}
	
	

}
