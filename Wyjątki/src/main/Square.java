package main;


public class Square {
	
	public static double square(int n) throws IllegalArgumentException{
		
		if(n<0){
			throw new IllegalArgumentException("Argument nie mo�e by� ujemny");
		}
		return Math.sqrt(n);
	}
	

}
