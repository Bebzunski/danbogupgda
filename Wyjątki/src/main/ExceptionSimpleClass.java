package main;

public class ExceptionSimpleClass {
	
	public void make(int a) throws Exception{
		
		if(a==55){
			throw new IllegalArgumentException("Niepoprawny argument");
		}
		
		if(a==5) {
			throw new Exception("a jest r�wne 5 !");
		}
			
		System.out.println("a jest poprawn� warto�ci�");
		
	}
	
	public void exceptionExample(String name) throws PawelException{
		if(name.equalsIgnoreCase("pawel")){
			throw new PawelException("jestem pawlem!");
		}
	}
}
