package main;

import java.util.LinkedList;

public class People {
	
	LinkedList<Person> lista = new LinkedList<>();
	
	private final String[] allowedEyes = {"black","blue","green","brown"};
	private final String[] allowedHair = {"black","blond","red","brown"};
	
	int max = 0;
	
	public void addPerson(Person per){
		lista.add(per);
	}
	
	public void addPerson(String name, String secondName, int age, String hair, String eyes, String shoes) throws BadHairException{
		
		try{
		lista.add(new Person(name, secondName, age, hair, eyes, shoes));
		} catch (WrongAgeException e){
			System.out.println(e.getMessage());
		}
		

	}
}
