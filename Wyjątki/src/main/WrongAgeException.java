package main;

public class WrongAgeException extends Exception{

	public WrongAgeException(String message) {
		super(message);
	}

	public WrongAgeException() {
		super("Wrong age exception");
	}
	

}
