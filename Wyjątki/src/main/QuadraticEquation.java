package main;

import java.util.InputMismatchException;
import java.util.Scanner;

public class QuadraticEquation {

	private Integer a;
	private Integer b;
	private Integer c;

	public QuadraticEquation(int a, int b, int c) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public QuadraticEquation() {
		super();
	}
	
	

	public Integer getA() {
		return a;
	}

	public void setA(Integer a) {
		this.a = a;
	}

	public Integer getB() {
		return b;
	}

	public void setB(Integer b) {
		this.b = b;
	}

	public Integer getC() {
		return c;
	}

	public void setC(Integer c) {
		this.c = c;
	}

	private Integer getNumber() {
		Integer in;
		Scanner sc = new Scanner(System.in);
		System.out.println("Podaj liczb� ca�kowit�");
		try{
		in = sc.nextInt();
		} catch(InputMismatchException e){
			in = 0;
		}
		sc.close();
		return in;
	}

	public double[] solve() throws ArithmeticException{
		
		if(a==null && b==null && c==null){
			a = getNumber();
			b = getNumber();
			c = getNumber();
		}

		double x1, x2;

		double delta = b * b - 4 * a * c;
		if (delta < 0) {
			throw new ArithmeticException("Delta wysz�a ujemna. R�wnanie nie ma rzowi�zania.");
		}
		x1 = (-b - Math.sqrt(delta)) / (2 * a);
		x2 = (-b + Math.sqrt(delta)) / (2 * a);

			return new double[] { x1, x2 };
		
	}

}
