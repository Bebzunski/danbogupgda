package figures;

import org.junit.Test;

public class FigureTest {
	
	@Test
	public void squareTest() {
		
		Square kwadrat = new Square(3);
		assert kwadrat.countArea() == 9;
		assert kwadrat.countCircumference() == 12;	

	}
	
	@Test
	public void circleTest() {
		
		Circle ko這 = new Circle(2);
		assert ko這.countArea() <12.6;
		assert ko這.countArea() >12.5;
		assert ko這.countCircumference() > 12.5;
		assert ko這.countCircumference() < 12.6;

	} 
	 public static void main(String[] args) {
		
		 Circle ko這2 = new Circle(4);
		 
		 show(ko這2);
		 
		 Square kwadrat = new Square(3);
		 show(kwadrat);
	}
	 
	 public static void show(Figure f){
		 System.out.println("Figura ma obw鏚 : "+f.countCircumference()+" , a pole : "+f.countArea());
	 }

}
