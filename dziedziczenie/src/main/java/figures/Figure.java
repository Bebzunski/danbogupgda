package figures;

public interface Figure {
	
	public double countArea();
	public double countCircumference();

}
