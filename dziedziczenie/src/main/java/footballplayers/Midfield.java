package footballplayers;

public class Midfield extends FootballPlayer{
	
	public Midfield(int number,String name,double height){
		super(number, name, height);
	}
	
	@Override
	public void sentence(){
		System.out.println("Striker, NR: "+this.getNumber()+" , Name : "+this.getName()+" , Height : "+this.getHeight());		

	}

}
