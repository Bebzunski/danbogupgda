package footballplayers;

public class Goalkeeper extends FootballPlayer{
	
	private int age;
	
	public Goalkeeper(int number, String name, double height,int age) {
		super(number,name,height);
		this.age=age;
	}
	@Override
		public void sentence(){
		System.out.println("Goalkeeper, NR: "+this.getNumber()+" , Name : "+this.getName()+" , Height : "+this.getHeight()+" , Age : "+age);		
		
		}
	
	public int getAge(){
		return age;
	}
	
	public void agePlusOne(){
		age++;
	}

	
}
