package footballplayers;

public class FootballPlayersMain {
	
	public static void main(String[] args) {
		
		
		FootballPlayer f1 = new Goalkeeper(12, "Buffon", 1.98, 39);
		FootballPlayer f2 = new Striker("Wawrzyniak", 1.78,7);
		FootballPlayer f3 = new Defender(15, "Krychowiak");
		FootballPlayer f4 = new Midfield(44, "Wawrzyniak", 1.99);
		
		
		FootballPlayer[] players = {f1,f2,f3,f4};
		for(FootballPlayer player : players){
			player.sentence();
			
		}
		
		
	}

}
