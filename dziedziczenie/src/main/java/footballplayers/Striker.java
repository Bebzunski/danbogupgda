package footballplayers;

public class Striker extends FootballPlayer{
	
	private int howManyGoals;

	public Striker(String name, double height, int howManyGoals) {
		super(9, name, height);
		this.howManyGoals = howManyGoals;
	
	}
	

	@Override
	public void sentence() {
		System.out.println("Striker, NR: "+this.getNumber()+" , Name : "+this.getName()+" , Height : "+this.getHeight());
		System.out.println("Scored goals : "+howManyGoals);
	}
	
	

}
