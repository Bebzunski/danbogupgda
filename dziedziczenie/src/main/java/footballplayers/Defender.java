package footballplayers;

public class Defender extends FootballPlayer{
	
	public Defender(int number, String name){
		super(number,name,1.89);
		
	}
	
	@Override
	public void sentence(){
		System.out.println("Defender , NR: "+this.getNumber()+" , Name : "+this.getName()+" , Height : "+this.getHeight());		

	}

}
