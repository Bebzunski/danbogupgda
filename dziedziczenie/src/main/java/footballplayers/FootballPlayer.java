package footballplayers;

public abstract class FootballPlayer {
	
	private int number;
	private String name;
	private double height;
	
	public FootballPlayer(int number, String name, double height) {
		
		this.number = number;
		this.name = name;
		this.height = height;
	}
	
	public abstract void sentence();
	
	public int getNumber() {
		return number;
	}

	public String getName() {
		return name;
	}

	public double getHeight() {
		return height;
	}
	

}
