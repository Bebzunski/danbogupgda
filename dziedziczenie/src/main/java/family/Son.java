package family;

public class Son {

	
	private String name;

	public Son() {
		super();
	}

	public Son(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
