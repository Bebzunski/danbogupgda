package family;

public class Mother {
	
	
	private String name;
	
	public Mother(String name){
		this.name=name;
	}
	
	public static void main(String[] args) {
		
		Mother m1 = new Mother("Janina");
		System.out.println(m1.getName());
		Mother m2 = new Mother("Gra�yna");
		System.out.println(m2.getName());
		m1.setName("Bo�ena");
		System.out.println(m1.getName());
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}

}
