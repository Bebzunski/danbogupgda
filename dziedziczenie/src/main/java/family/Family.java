package family;

public class Family {
	
	public static void main(String[] args) {
		Mother mother = new Mother("Grazyna");
		introduce(mother);
	}
	
	public static void introduce(Mother mother){
		System.out.println("I'm a mother. My name is " + mother.getName());
	}
	
	public static void introduce(Father father){
		System.out.println("I'm a mother. My name is " + father.getName());
	}
	
	public static void introduce(Son son){
		System.out.println("I'm a mother. My name is " + son.getName());
	}
	
	public static void introduce(Daugther daughter){
		System.out.println("I'm a mother. My name is " + daughter.getName());
	}

}
