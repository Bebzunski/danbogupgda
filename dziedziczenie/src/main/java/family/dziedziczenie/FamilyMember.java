package family.dziedziczenie;

public abstract class FamilyMember {
	
	private String name;

	public FamilyMember(String name) {
		super();
		this.name = name;
	}
	
	public abstract void introduce();


	public String getName(){
		return name;
	}
}
