package family.dziedziczenie;

public class Family {
	
	public static void main(String[] args) {
		
		Mother mother = new Mother("Grazyna");
//		mother.introduce();
		
		Father father = new Father("Janusz");
//		father.introduce();
		
		Son son = new Son("Daniel");
//		son.introduce();
		
		Daugther daugther = new Daugther("Anna");
//		daugther.introduce();
		
		FamilyMember[] members = {mother, father, son , daugther};
		
		for(FamilyMember X : members){
			X.introduce();
		}
	}
		


}
