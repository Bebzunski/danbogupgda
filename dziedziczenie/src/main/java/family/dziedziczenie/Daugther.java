package family.dziedziczenie;

public class Daugther extends FamilyMember{
	

	public Daugther(String name){
		super(name);
	}
	
	@Override
	public void introduce(){
		System.out.println("I'm a daugther. My name is " + this.getName());
	}
}
