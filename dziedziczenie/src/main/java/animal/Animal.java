package animal;

public interface Animal {

		String makeNoise();
}
