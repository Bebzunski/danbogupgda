package xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ParseStudentXML {

	static void printStudentsInfo(Document doc) {
		NodeList students = doc.getElementsByTagName("student");
		System.out.println("----------------------------");
		for (int i = 0; i < students.getLength(); i++) {
			Node student = students.item(i);
			ParseStudentXML.printStudentInfo(student);
		}
	}

	static void printStudentInfo(Node studentNode) {
		System.out.println("\nCurrent Element :" + studentNode.getNodeName());
		if (studentNode.getNodeType() == Node.ELEMENT_NODE) {
			
			Element student = (Element) studentNode;
			
			System.out.println("Student roll no : " + student.getAttribute("rollno"));

		}
	}

	public static void main(String[] args) {
	
		try {
	
			Document doc = ParseXML.parseFile(ParseXML.FILE_NAME);
	
			printStudentsInfo(doc);
	
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}

}
