package students;

import static xml.ParseXML.getText;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import xml.ParseXML;


public class XMLFile implements IFile {
	

	@Override
	public void save(List<Student> studentList) {
	
		try {
			
			Document doc = ParseXML.newDocument();

			createContent(doc, studentList);

			ParseXML.saveFile(doc, "students.xml");
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createContent(Document doc, List<Student> studentList) {
		
		Element students = doc.createElement("students");
		doc.appendChild(students);
		
		for(Student student : studentList){
			
			Element studentElement = doc.createElement("student");
			
			ParseXML.setAttribute(doc, studentElement, "index", String.valueOf(student.getNumerIndeksu()));
			
			Element studentName = doc.createElement("name");
			studentName.appendChild(doc.createTextNode(student.getImie()));
			studentElement.appendChild(studentName);
			
			Element studentSecondName = doc.createElement("surname");
			studentSecondName.appendChild(doc.createTextNode(student.getNazwisko()));
			studentElement.appendChild(studentSecondName);

			students.appendChild(studentElement);
		}
		
	}
	

	@Override
	public List<Student> load() {
		
		List<Student> studentList = new ArrayList<>();
		
		try {
			Document doc = ParseXML.parseFile("students.xml");
			NodeList students = doc.getElementsByTagName("students");
			
			
			for (int i = 0; i < students.getLength(); i++) {
				Node studentNode = students.item(i);
				Student s = parse(studentNode);
				studentList.add(s);
				
			}
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return studentList;
	}

	private Student parse(Node studentNode) {
		
		Element student = (Element) studentNode;
		Student s = new Student(Integer.parseInt(student.getAttribute("index")),getText(student,"name"),getText(student,"surname"));
		return s;
	}
}
