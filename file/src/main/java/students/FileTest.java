package students;

import java.util.ArrayList;
import java.util.List;


public class FileTest {
	
	public static void main(String[] args) {
		
		
		ArrayList<Student> studentsList = new ArrayList<>();
		studentsList.add(new Student(123, "Daniel", "Bogdaalski"));
		studentsList.add(new Student(1232, "Marek", "Nowak"));
		studentsList.add(new Student(1234, "Jacek", "Ptak"));
		studentsList.add(new Student(1235, "Ania", "Kowalska"));
		
		University u = new University(studentsList);
		
		IFile tf = new XMLFile();
		
		tf.save(u.getStudentList());
		
		for(Student s : tf.load()){
			System.out.println(s);
		}
		
		System.exit(0);
		
		
		List<Student> studentsFromFile = tf.load();
		University universityFromFile = new University(studentsFromFile);
		
		universityFromFile.showAll();
		
		
		
	}

}
