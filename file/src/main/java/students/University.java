package students;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class University {
	
	private Map<Integer,Student> students = new HashMap<>();
	
	
	
	public University() {
		super();
	}
	
	public University(List<Student> studentsList){
		
		for(int i = 0 ; i < studentsList.size();i++){
		students.put(studentsList.get(i).getNumerIndeksu(), studentsList.get(i));
		}
	}
	
	

	public void addStudent(int indexNumber, String name, String surname){
		Student student = new Student(indexNumber,name,surname);
		students.put(student.getNumerIndeksu(),student);
	}
	
	public boolean studentExists(int indexNumer){
		
		return students.containsKey(indexNumer);
	}
	
	public Student getStudent(int indexNumber){
		return students.get(indexNumber);
	}	
	public int studentsNumber(){
		return students.size();
	}
	
	public void showAll(){
		for(Student s : students.values()){
			System.out.println(s.getNumerIndeksu()+" "+ s.getImie()+" "+s.getNazwisko());
		}
	}
	
	public List<Student> getStudentList(){
		
		ArrayList<Student> studentList = new ArrayList<>();
		for(Student st : students.values()){
			studentList.add(st);
		}
		
		return studentList;
	}
	
	public static void main(String[] args) {
		
		ArrayList<Student> studentsList = new ArrayList<>();
		studentsList.add(new Student(123, "daniel", "bogda"));
		studentsList.add(new Student(1232, "daniel", "bogda1"));
		studentsList.add(new Student(1234, "daniel", "bogda2"));
		studentsList.add(new Student(1235, "daniel", "bogda3"));
		
		University u = new University(studentsList);
		u.showAll();
		
		for(Student s : u.getStudentList()){
			System.out.println("na liscie : "+s);
		}
		
		}
//		u.addStudent(100, "Jan", "asdas");
		
		
	

}