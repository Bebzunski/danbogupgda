package students;

import java.io.Serializable;

public class Student implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer numerIndeksu;
	private String imie;
	private String nazwisko;
	
	public Student(Integer numerIndeksu, String imie, String nazwisko) {
		super();
		this.numerIndeksu = numerIndeksu;
		this.imie = imie;
		this.nazwisko = nazwisko;
	}

	public Integer getNumerIndeksu() {
		return numerIndeksu;
	}

	public String getImie() {
		return imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	@Override
	public String toString() {
		return numerIndeksu+" "+imie+" "+" "+nazwisko;
	}	
	


}