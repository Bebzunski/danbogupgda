package students;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TextFile implements IFile {

	private final static String FILE_NAME = "students.txt";

	/* (non-Javadoc)
	 * @see students.IFile#save(java.util.List)
	 */
	@Override
	public void save(List<Student> studentList) {

		try {
			PrintStream ps = new PrintStream("src/main/resources/"+"text_" + FILE_NAME);
			for (Student s : studentList) {
				ps.println(s.getImie() + " " + s.getNazwisko() + " " + s.getNumerIndeksu());
			}
			ps.close();
		} catch (FileNotFoundException e) {
			System.out.println("nie ma pliku");
		}
	}

	/* (non-Javadoc)
	 * @see students.IFile#load()
	 */
	@Override
	public List<Student> load() {
		
		List<Student> students = new ArrayList<>();
	try{
		Scanner scanner = new Scanner(new BufferedInputStream(new FileInputStream("src/main/resources/" +"text_" + FILE_NAME)));
		while(scanner.hasNextLine()){
		String[] arrrOfStudent = scanner.nextLine().split(" ");
		students.add(new Student(Integer.parseInt(arrrOfStudent[2]),arrrOfStudent[0],arrrOfStudent[1]));
		}
		scanner.close();
	}  catch (FileNotFoundException e) {
		e.printStackTrace();
	}
		return students;
	}

}
