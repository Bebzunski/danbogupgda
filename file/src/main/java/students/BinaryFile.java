package students;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BinaryFile implements IFile {

	private final static String FILE_NAME = "students_text.txt";

	@Override
	public void save(List<Student> studentList) {

		try {
			FileOutputStream fos = new FileOutputStream("src/main/resources/" +"binary_"+ FILE_NAME);
			BufferedOutputStream bouf = new BufferedOutputStream(fos);
			DataOutputStream out = new DataOutputStream(bouf);
			
			for(Student student : studentList){
				out.writeInt(student.getNumerIndeksu());
				out.writeUTF(student.getImie());
				out.writeUTF(student.getNazwisko());
				
			}
			out.close();
		} catch (FileNotFoundException e) {
			System.out.println("nie ma pliku");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public List<Student> load() {
		
		List<Student> students = new ArrayList<>();
		
		try (
			FileInputStream fileInputStream = new FileInputStream("src/main/resources/" +"binary_"+ FILE_NAME);
			DataInputStream in = new DataInputStream(new BufferedInputStream(fileInputStream)))
		{
			
			while(in.available()>0){

				students.add(new Student(in.readInt(),in.readUTF(),in.readUTF()));
				
			}
		} catch (FileNotFoundException e) {
			System.out.println("ni ma niczego");
			e.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		return students;
	}

}
