package students;

import java.util.List;

public interface IFile {

	void save(List<Student> studentList);

	List<Student> load();

}