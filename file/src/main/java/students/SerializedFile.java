package students;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class SerializedFile implements IFile {

	private final static String FILE_NAME = "students.txt";

	@Override
	public void save(List<Student> studentList) {
		try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("src/main/resources/"+"serialize_"+ FILE_NAME))) {
			out.writeObject(studentList);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public List<Student> load() {
		try (ObjectInputStream in = new ObjectInputStream(new FileInputStream("src/main/resources/" +"serialize_"+ FILE_NAME))){
			List<Student> students = new ArrayList<>();
			return students;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return null;
	}

}
