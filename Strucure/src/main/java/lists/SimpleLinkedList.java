package lists;


public class SimpleLinkedList implements SimpleList {

    private Element first;
    private Element last;
    private int number = 0;

    public void add(int value) {
        Element el = new Element();
        el.value = value;
        if(first==null){
            first = el;
            last = el ;
        } else {
            el.prev = last;
            last.next = el;
            last = el;
        }
        number++;
    }

    public void add(int value, int index) {

        Element el = new Element();
        el.value = value;
        if(index > number){
            throw new IndexOutOfBoundsException();
        }
            Element currentElement = first;
            for (int i = 1; i < index; i++) {
                currentElement = currentElement.next;
            }
            el.prev = currentElement;
            el.next = currentElement.next;
            currentElement.next = el;

        number++;
    }

    public boolean contain(int value) {
        Element currentElement = first;
        for(int i = 0 ; i < number ; i++){
           if (currentElement.value == value){
               return true;
           }
           currentElement = currentElement.next;
        }
        return false;
    }

    public void remove(int index) {

        if(index>=number) {
            throw new IndexOutOfBoundsException();
        }
        if(index==0){
            first = first.next;
            first.prev = null;
        } else if(index == number - 1){
            last = last.prev;
            last.next = null;
        } else {
            Element leftElement = first;
            for (int i = 1; i < index; i++) {
                leftElement = leftElement.next;
            }
            Element rightElement = leftElement.next.next;
            leftElement.next = rightElement;
            rightElement.prev = leftElement;
        }
        number--;
    }

    public void removeValue(int value) {
        Element element = first;
        int i = 0;
        while(element.value != value &&  element.next != null){
            element = element.next;
            i++;
        }
        if(i<number-1){
            remove(i);
        } else if(element.value == value){
            remove(i);
        }
     }

    public int size() {
        return number;
    }

    public int get(int index) {
        Element currentElement = first;
        for(int i = 0 ; i < index ; i++){
            currentElement = currentElement.next;
        }
        return currentElement.value;
    }

    private static class Element {
        int value;
        Element next;
        Element prev;
    }
}
