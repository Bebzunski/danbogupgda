package kolejka;


import lists.SimpleArrayList;

import java.util.NoSuchElementException;

public class SimpleArrayQueue implements SimpleQueue {

    private int[] data;
    private int poczatek = 0;
    private int koniec = -1;
    private boolean isFull = false;

    @Override
    public boolean isEmpty() {
        return ((poczatek)%data.length == (koniec + 1)%data.length) && !isFull;
    }

    public SimpleArrayQueue() {
        this(5);
    }

    public SimpleArrayQueue(int size) {
        data = new int[size];
    }

    @Override
    public void offer(int value) {
        if (isFull) {
            int[] biggerData = new int[2 * data.length];
            int i = 0;
            while(!isEmpty()) {
                biggerData[i] = pull();
                i++;
            }
                poczatek = 0;
                koniec = data.length - 1;
                isFull = false;
                data = biggerData;
    }

        koniec = (koniec + 1) % data.length;
        data[koniec] = value;

        isFull = (poczatek%data.length == (koniec + 1)%data.length);
    }

    @Override
    public int pull() {
        if(isEmpty()){
            throw new NoSuchElementException();
        }
        int el = data[poczatek];
        poczatek = (poczatek + 1) % data.length;
        isFull = false;
        return el;
    }

    @Override
    public int peek() {
        if (isEmpty()) {
            throw new NoSuchElementException("kolejka jest pusta");
        }

        return data[poczatek];
    }

    public boolean isFull() {
        return isFull;
    }

}
