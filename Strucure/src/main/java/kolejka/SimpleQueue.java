package kolejka;

public interface SimpleQueue {

    boolean isEmpty();

    void offer(int value);

    int pull();

    int peek();

}

