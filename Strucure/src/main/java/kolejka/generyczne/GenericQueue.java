package kolejka.generyczne;

public interface GenericQueue<T> {

    boolean isEmpty();

    void offer(T value);

    T pull();

    T peek();

}

