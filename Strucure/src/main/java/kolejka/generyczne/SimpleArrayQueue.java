package kolejka.generyczne;

import java.util.NoSuchElementException;

public class SimpleArrayQueue<T> implements GenericQueue<T> {

    private Object[] data;
    private int poczatek = 0;
    private int koniec = -1;
    private boolean isFull = false;

    @Override
    public boolean isEmpty() {
        return ((poczatek)%data.length == (koniec + 1)%data.length) && !isFull;
    }

    public SimpleArrayQueue() {
        this(5);
    }

    public SimpleArrayQueue(int size) {
        data = new Object[size];
    }

    @Override
    public void offer(T value) {
        if (isFull) {
            Object[] biggerData = new Object[2 * data.length];
            int i = 0;
            while(!isEmpty()) {
                biggerData[i] = pull();
                i++;
            }
                poczatek = 0;
                koniec = data.length - 1;
                isFull = false;
                data = biggerData;
    }

        koniec = (koniec + 1) % data.length;
        data[koniec] = value;

        isFull = (poczatek%data.length == (koniec + 1)%data.length);
    }

    @Override
    public T pull() {
        if(isEmpty()){
            throw new NoSuchElementException();
        }
        T el = (T) data[poczatek];
        data[poczatek]=null;
        poczatek = (poczatek + 1) % data.length;
        isFull = false;
        return el;
    }

    @Override
    public T peek() {
        if (isEmpty()) {
            throw new NoSuchElementException("kolejka jest pusta");
        }

        return (T) data[poczatek];
    }

    public boolean isFull() {
        return isFull;
    }

}
