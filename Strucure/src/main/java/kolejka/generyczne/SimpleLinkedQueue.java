package kolejka.generyczne;


import java.util.NoSuchElementException;

public class SimpleLinkedQueue<T> implements GenericQueue<T> {

    private Element<T> first;
    private Element<T> last;

    @Override
    public boolean isEmpty() {
        return first == null;
    }

    @Override
    public void offer(T value) {
        Element el = new Element();
        el.value = value;
        if(isEmpty()){
            first = el;
            last = el;
        }
        last.next = el;
        last = el;
    }

    @Override
    public T pull() {

        T result = peek();
        if(first == last){
            first = null;
            last = null;
        }
        first = first.next;
        return result;
    }

    @Override
    public T peek() {
        if(isEmpty()){
            throw new NoSuchElementException();
        }
        return first.value;
    }

    private static class Element<T> {
        T value;
        Element next;
    }
}
