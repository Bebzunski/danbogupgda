package kolejka;


import java.util.NoSuchElementException;

public class SimpleLinkedQueue implements SimpleQueue {

    private Element first;
    private Element last;

    @Override
    public boolean isEmpty() {
        return first == null;
    }

    @Override
    public void offer(int value) {
        Element el = new Element();
        el.value = value;
        if(isEmpty()){
            first = el;
            last = el;
        }
        last.next = el;
        last = el;
    }

    @Override
    public int pull() {

        int result = peek();
        first = first.next;
        if(first == null ){
            last = null;
        }
        return result;
    }

    @Override
    public int peek() {
        if(isEmpty()){
            throw new NoSuchElementException();
        }
        return first.value;
    }

    private static class Element {
        int value;
        Element next;
    }
}
