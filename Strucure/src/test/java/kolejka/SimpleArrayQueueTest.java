package kolejka;

import org.junit.Before;
import org.junit.Test;

import java.util.NoSuchElementException;

import static org.junit.Assert.*;


public class SimpleArrayQueueTest {

    private SimpleArrayQueue queue;

    @Before
    public void init(){
        queue = new SimpleArrayQueue();
    }

    @Test
    public void whenEmptyQueueTrueExpected() throws Exception {
        assert queue.isEmpty() == true ;
    }

    @Test
    public void whenFullQueueFalseExpected() throws Exception {
        queue.offer(4);
        queue.offer(-5);
        queue.offer(3);
        queue.offer(7);
        queue.offer(9);
        assert queue.isEmpty() == false;
        assert queue.isFull() == true ;
    }
    @Test
    public void whenNotEmptyQueueFalseExpected() throws Exception {
        queue.offer(4);
        queue.offer(-5);

        assert queue.isEmpty() == false ;
        assert queue.isFull() == false ;
    }
    @Test
    public void whenAddAndPullTrueExpected() throws Exception {
        queue.offer(4);
        queue.offer(-5);
        queue.pull();
        queue.pull();

        assert queue.isEmpty() == true ;
    }

    @Test
    public void whenCrowdedQueuue() throws Exception {
        queue.offer(5);
        queue.offer(9);
        queue.offer(8);
        queue.offer(3);
        queue.offer(-5);

        assert queue.isFull() == true;

        queue.offer(-9);
        queue.offer(1);
        int[] expected = {5,9,8,3,-5,-9,1};

        for(int el : expected){
            assertEquals(el, queue.pull());
        }
    }
    @Test
    public void whenOfferToQueuueCurrentValueExpected() throws Exception {
        queue.offer(5);
        queue.offer(9);
        queue.offer(8);
        queue.offer(3);

        int[] expected = {5,9,8,3};

        for(int el : expected){
            assertEquals(el, queue.pull());
        }
    }

    @Test
    public void pull() throws Exception {
    }

    @Test
    public void whenPullElementsFirstElementExpected() throws Exception {
        queue.offer(5);
        queue.offer(9);
        queue.offer(8);
        queue.offer(3);
        queue.pull();
        queue.pull();
        assert queue.peek() == 8;
    }

    @Test
    public void whenAddElementFirstElementExpected() throws Exception {
        queue.offer(5);
        queue.offer(9);
        assert queue.peek() == 5;
    }

    @Test(expected = NoSuchElementException.class)
    public void whenEmptyQueueExceptionExpected() throws Exception {
        queue.peek();
    }

    @Test(expected = NoSuchElementException.class)
    public void whenEmptyQueueMethodPullExceptionExpected() throws Exception {
        queue.pull();
    }

    @Test
    public void when111OfferAnd111Pull() throws Exception {
        for(int i = 0 ; i < 111 ; i++){
            queue.offer(1);
        }

        for(int i = 0 ; i < 111 ; i++){
            queue.pull();
        }

        assertEquals(true , queue.isEmpty());
    }

}