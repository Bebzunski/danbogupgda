package usergenerator;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Random;
import java.util.Scanner;

public class UserGenerator {

	private final String path = "resources/";
	// private User currentUser;

	public User getRandomUser() {
		// currentUser = new User();
		UserSex uSex = UserSex.SEX_MALE;
		if (new Random().nextInt(2) == 0) {
			uSex = UserSex.SEX_FEMALE;
		}
		return getRandomUser(uSex);
	}

	public User getRandomUser(UserSex sex) {
		User currentUser = new User();
		currentUser.setSex(sex);
		String name = "";
		if (sex.equals(UserSex.SEX_MALE)) {
			name = getRandomLineFromFile("name_m.txt");
		} else {
			name = getRandomLineFromFile("name_f.txt");
		}
		currentUser.setName(name);
		currentUser.setSecondName(getRandomLineFromFile("lastname.txt"));
		currentUser.setPhone(getRandomPhoneNumber());
		currentUser.setcNN(getRandomCCN());
		currentUser.setAddress(getRandomAddress());

		String date = getRandomBirthDate();
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

		try {
			currentUser.setBirthDate(sdf.parse(date));
		} catch (ParseException e) {
			System.out.println(e.getMessage());
		}

		currentUser.setpESEL(getRandomPesel(currentUser));
		return currentUser;
	}

	public int countLines(String filename) {
		int lines = 0;
		try (Scanner sc = new Scanner(new File(path + filename))) {
			while (sc.hasNextLine()) {

				lines++;
				sc.nextLine();
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lines;
	}

	private String getRandomLineFromFile(String filename) {
		Random r = new Random();
		String currentLine = "";
		int allLines = countLines(filename);
		try (Scanner sc = new Scanner(new File(path + filename))) {
			int randomLine = r.nextInt(countLines(filename)) + 1;
			for (int i = 1; i <= allLines; i++) {
				currentLine = sc.nextLine();
				if (i == randomLine) {
					return currentLine;
				}
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return currentLine;
	}

	private String getRandomPhoneNumber() {
		Random r = new Random();
		String phoneNumber = "";
		for (int i = 1; i <= 11; i++) {
			if (i % 4 == 0) {
				phoneNumber += "-";
			} else {
				phoneNumber += r.nextInt(10);
			}
		}
		return phoneNumber;
	}

	private String getRandomCCN() {
		Random r = new Random();
		String cCnNumber = "";
		for (int i = 1; i <= 19; i++) {
			if (i % 5 == 0) {
				cCnNumber += " ";
			} else {
				cCnNumber += r.nextInt(10);
			}
		}
		return cCnNumber;
	}

	private String getRandomBirthDate() {
		int[] dayInMonth = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		int year = 1890 + new Random().nextInt(120);
		int month = new Random().nextInt(12) + 1;
		if (year % 4 == 0) {
			dayInMonth[1]++;
		}
		int day = new Random().nextInt(dayInMonth[month - 1]) + 1;

		return pasteZero(day) + "." + pasteZero(month) + "." + year;
	}

	private String pasteZero(int a) {

		String s = "";
		if (a < 10) {
			s = "0" + a;
		} else {
			s = "" + a;
		}
		return s;
	}

	private Address getRandomAddress() {
		Address address = new Address();
		String cityAndZipCode = getRandomLineFromFile("city.txt");
		String[] ArrayOfCityAndZipcode = cityAndZipCode.split("\t");
		address.setCity(ArrayOfCityAndZipcode[0].trim());
		address.setZipcode(ArrayOfCityAndZipcode[1].trim());
		address.setCountry("Polska");

		String street = getRandomLineFromFile("street.txt");
		address.setStreet(street.trim());
		address.setNumber("" + (new Random().nextInt(100) + 1));

		return address;
	}

	private String getRandomPesel(User currentUser) {
		
		String pesel = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd");
		String[] arrOfDate = sdf.format(currentUser.getBirthDate()).split(" ");
		pesel += arrOfDate[0].substring(2);
		int year = Integer.parseInt(arrOfDate[0]);
		int month = Integer.parseInt(arrOfDate[1]);

		if (year < 1900) {
			month += 80;
		} else if (year > 1999) {
			month += 20;
		}
		pesel += pasteZero(month);

		pesel += arrOfDate[2];
		int sevPesel = new Random().nextInt(10);
		int eightPesel = new Random().nextInt(10);
		int ninePesel = new Random().nextInt(10);
		int sexNumber;
		if (currentUser.getSex().equals(UserSex.SEX_FEMALE)) {
			sexNumber = 0;
		} else {
			sexNumber = 1;
		}

		int tenthPesel = 2*(new Random().nextInt(5))+sexNumber;
		pesel = pesel + sevPesel + eightPesel + ninePesel+tenthPesel;
		String[] arrOfPeselNumbers = pesel.split("");
		
		int[] wagi = {9 , 7 , 3 , 1 , 9 , 7 , 3 , 1 , 9 , 7};
		int checkNumber = 0;
		for(int i = 0 ; i <10 ; i++){
			checkNumber+=Integer.parseInt(arrOfPeselNumbers[i])*wagi[i];
		}
		
		int elevenPesel = checkNumber%10;
		
		return pesel+elevenPesel;
	}
}
