package usergenerator;

public enum UserSex {
	
	SEX_MALE("M�czyzna"),
	SEX_FEMALE("Kobieta"),
	SEX_UNDEFINED("Nieokre�lono");
	
	private String name;

	private UserSex(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
