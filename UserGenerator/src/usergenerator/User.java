package usergenerator;

import java.text.SimpleDateFormat;
import java.util.Date;

public class User {

	private String name;
	private String secondName;
	private String phone;
	private String cNN;
	private String pESEL;
	private Date birthDate; // dd.mm.yyyy
	private Address address;
	private UserSex sex;

	public User() {
		super();
	}

	public User(String name, String secondName, String phone, String cNN, String pESEL, Date birthDate, Address address,
			UserSex sex) {
		super();
		this.name = name;
		this.secondName = secondName;
		this.phone = phone;
		this.cNN = cNN;
		this.pESEL = pESEL;
		this.birthDate = birthDate;
		this.address = address;
		this.sex = sex;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getcNN() {
		return cNN;
	}

	public void setcNN(String cNN) {
		this.cNN = cNN;
	}

	public String getpESEL() {
		return pESEL;
	}

	public void setpESEL(String pESEL) {
		this.pESEL = pESEL;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public UserSex getSex() {
		return sex;
	}

	public void setSex(UserSex sex) {
		this.sex = sex;
	}

	@Override
	public String toString() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		return name + " " + secondName + "\np�e�: " + sex.getName() +
				"\ntelefon: " + phone + "\ncNN: " + cNN + "\nPESEL: "
				+ pESEL + "\ndata urodzenia: " + sdf.format(birthDate) + "\n\nAdres: " + address;
	}

}
