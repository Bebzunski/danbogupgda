package pl.org.pfig.tdd;

public class Excercise {

	public int absoluteValue(int num) {
		if (num < 0) {
			num = -num;
		}
		return num;
	}

	public String determineDayOfWeek(int day) throws IllegalArgumentException {
		if (day < 1 || day > 7) {
			throw new IllegalArgumentException("Bad input value");
		}
		String[] days = { "Niedziela", "Poniedzialek", "Wtorek", "Sroda", "Czwartek", "Piatek", "Sobota" };

		return days[day - 1];
	}

	public String determineMonthOfYear(int month) throws IllegalArgumentException {
		if (month < 1 || month > 12) {
			throw new IllegalArgumentException();
		}

		String[] months = { "Styczen", "Luty", "Marzec", "Kwwiecien", "Maj", "Czerwiec", "Lipiec", "Sierpien",
				"Wrzesien", "Pazdziernik", "Listopad", "Grudzien" };

		return months[month - 1];
	}

	public int getTimeInSeconds(int hours, int minutes, int seconds) throws IllegalArgumentException {
		if (hours < 0 || minutes < 0 || seconds < 0) {
			throw new IllegalArgumentException();
		}
		return hours * 3600 + minutes * 60 + seconds;
	}

	public int getGreatestCommonDivisor(int a, int b) throws IllegalArgumentException {

		if (a == 0 || b == 0) {
			throw new IllegalArgumentException("IllegalArgumentExceptiond");
		}
		int bigger = absoluteMax(a, b);
		int smaller = absoluteMin(a, b);
		int diff = 0;
		while (bigger != smaller) {

			diff = bigger - smaller;
			bigger = absoluteMax(smaller, diff);
			smaller = absoluteMin(smaller, diff);
		}

		return bigger;
	}

	public int absoluteMax(int a, int b) {
		return Math.max(Math.abs(a), Math.abs(b));
	}

	public int absoluteMin(int a, int b) {
		return -Math.max(-Math.abs(a), -Math.abs(b));
	}

	public int[] processNumber(int a, int b) throws IllegalArgumentException {

		if (!isInRange(a, 0, 255)) {

			throw new IllegalArgumentException("Liczba 'a' powinna by� w zakresie [0 , 255] !");
		}

		if (!isInRange(b, a, 255)) {
			throw new IllegalArgumentException("Liczba 'b' powinna by� w zakresie [" + a + "] , 255] !");
		}

		int numberOfElement = b - a + 1;
		int[] result = new int[numberOfElement];

		boolean firtsOdd = (a % 2) == 1;
		boolean lastOdd = (b % 2) == 1;

		int numberOfOdd = numberOfElement / 2 + boolToInt(firtsOdd && lastOdd);

		int numberOfEven = numberOfElement - numberOfOdd;

		int firstEvenElement = a + boolToInt(firtsOdd);
		putElementsOfSequence(result, 0, firstEvenElement, 2, numberOfEven);

		int lastOddNumber = b - boolToInt(!lastOdd);
		putElementsOfSequence(result, numberOfEven, lastOddNumber, -2, numberOfOdd);

		return result;

	}

	public boolean isInRange(int arg, int left, int right) {
		return (arg >= left && arg <= right);
	}

	public void putElementsOfSequence(int[] array, int startingIndex, int startingValue, int difference, int n) {
		int value = startingValue;
		for (int i = startingIndex; i < startingIndex + n; i++) {
			array[i] = value;
			value += difference;
		}
	}

	public int boolToInt(boolean bool) {

		if (bool) {
			return 1;
		}
		return 0;
	}

}
