package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DetermineDayOfWeekTest {

	private Excercise e;

	@Before
	public void init() {
		e = new Excercise();
	}

	@Test
	public void whenNegativeOutRangeArgumentIsGivenIllegalArgumentExceptionExpected() {

		int arg = -14;
		IllegalArgumentException iae = null;
		try {
			e.determineDayOfWeek(arg);
		} catch (IllegalArgumentException e) {
			iae = e;
		}
		assertNotNull("Wyjatek nie wystapi�", iae);
	}

	@Test(expected = IllegalArgumentException.class)
	public void whenPositiveOutRangeArgumentIsGivenIllegalArgumentExceptionExpected() {

		int arg = 123;

		e.determineDayOfWeek(arg);
	}

	@Test
	public void whenProperValueIsGivenProperDayOfWeekExpected() {
		String[] expected = { "Niedziela", "Poniedzialek", "Wtorek", "Sroda", "Czwartek", "Piatek", "Sobota" };
		for (int i = 0; i < 7; i++) {
			assertEquals(true, expected[i].equals(e.determineDayOfWeek(i + 1)));
		}
	}

}
