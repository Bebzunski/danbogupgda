package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GreatestCommonDivisorTest {
	
	private Excercise ex;
	
	@Before
	public void init(){
		ex = new Excercise();
	}

	@Test(expected=IllegalArgumentException.class)
	public void whenZerosIsGivenExceptionExpected() {
		int a = 0;
		int b = 0;
		ex.getGreatestCommonDivisor(a, b);
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void whenFirstZeroIsGivenExceptionExpected() {
		int a = 0;
		int b = 6;
		ex.getGreatestCommonDivisor(a, b);
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void whenSecondZeroIsGivenExceptionExpected() {
		int a = 9;
		int b = 0;
		ex.getGreatestCommonDivisor(a, b);
		
	}
	
	@Test
	public void whenFirstNegativeIsGivenProperValueExpected() {
		int a = -32;
		int b = 6;
		assertEquals(2, ex.getGreatestCommonDivisor(a, b) );
		
	}
	
	@Test
	public void whenSecondNegativeIsGivenProperValueExpected() {
		int a = 9;
		int b = -21;
		assertEquals(3, ex.getGreatestCommonDivisor(a, b) );
	}
	
	@Test
	public void whenFirstBiggerIsGivenProperValueExpected() {
		int a = 66;
		int b = 23;
		assertEquals(1, ex.getGreatestCommonDivisor(a, b) );
	}
	
	@Test
	public void whenSecondBiggerIsGivenProperValueExpected() {
		int a = 14;
		int b = 21;
		assertEquals(7, ex.getGreatestCommonDivisor(a, b) );
	}
	
	@Test
	public void whenArrgAreEqualIsGivenProperValueExpected() {
		int a = 66;
		int b = 66;
		assertEquals(66, ex.getGreatestCommonDivisor(a, b) );
	}

}
