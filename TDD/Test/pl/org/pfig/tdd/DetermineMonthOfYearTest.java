package pl.org.pfig.tdd;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;

public class DetermineMonthOfYearTest {
	
	private Excercise ex;
	
	@Before
	public void init(){
		ex = new Excercise();
	}

	@Test
	public void whenNegativeArgumentOutOfRangeIsGivenExceptionExpected() {
		int arg = -2;
		IllegalArgumentException iae = null;
		try{
			ex.determineMonthOfYear(arg);
		} catch (IllegalArgumentException e){
			iae = e;
		}
		assertNotNull("IllegalArgumentException" , iae);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void whenPositiveArgumentOutOfRangeIsGivenExceptionExpected(){
		int arg = 23;
		ex.determineMonthOfYear(arg);
	}
	@Test
	public void whenProperValueIsGivenProperMonthIsExpected(){
		int[] args = {1,12};
		String[] expecteds = {"Styczen","Grudzien"};
		for(int i = 0 ; i <args.length ; i++){
			assertEquals(true, expecteds[i].equals(ex.determineMonthOfYear(args[i])));
		}
	}
	
	@Test
	public void whenMidleValueIsGivenProperMonthIsExpected(){
		int arg = 6;
		String expected = "Czerwiec";
		
			assertEquals(expected, ex.determineMonthOfYear(arg));
	}

}
