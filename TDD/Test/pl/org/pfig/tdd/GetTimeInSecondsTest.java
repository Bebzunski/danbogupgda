package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GetTimeInSecondsTest {

	private Excercise ex;

	@Before
	public void init() {
		ex = new Excercise();
	}

	@Test
	public void whenHoursNegativeIsGivenExceptionExpected() {
		int hours = -5;
		int minutes = 9;
		int seconds = 14;
		IllegalArgumentException iae = null;
		try {
			ex.getTimeInSeconds(hours, minutes, seconds);
		} catch (IllegalArgumentException e) {
			iae = e;
		}
		assertNotNull("IllegalArgumentException", iae);
	}

	@Test
	public void whenMinutesNegativeIsGivenExceptionExpected() {
		int hours = 5;
		int minutes = -9;
		int seconds = 14;
		IllegalArgumentException iae = null;
		try {
			ex.getTimeInSeconds(hours, minutes, seconds);
		} catch (IllegalArgumentException e) {
			iae = e;
		}
		assertNotNull("IllegalArgumentException", iae);
	}

	@Test
	public void whenSecondsNegativeIsGivenExceptionExpected() {
		int hours = 5;
		int minutes = 9;
		int seconds = -14;
		IllegalArgumentException iae = null;
		try {
			ex.getTimeInSeconds(hours, minutes, seconds);
		} catch (IllegalArgumentException e) {
			iae = e;
		}
		assertNotNull("IllegalArgumentException", iae);
	}

	@Test
	public void whenValuesIsGivenProperTimeExpected() {
		int hours = 3;
		int minutes = 5;
		int seconds = 23;
		assertEquals(11123, ex.getTimeInSeconds(hours, minutes, seconds));
	}
	
	@Test
	public void whenZerosIsGivenZeroTimeExpected() {
		int hours = 0;
		int minutes = 0;
		int seconds = 0;
		assertEquals(0, ex.getTimeInSeconds(hours, minutes, seconds));
	}
	
	@Test
	public void whenHoursEqualsZeroIsGivenProperTimeExpected() {
		int hours = 0;
		int minutes = 13;
		int seconds = 3;
		assertEquals(783, ex.getTimeInSeconds(hours, minutes, seconds));
	}
	
	@Test
	public void whenMinutesEqualsZeroIsGivenProperTimeExpected() {
		int hours = 44;
		int minutes = 0;
		int seconds = 89;
		assertEquals(158489, ex.getTimeInSeconds(hours, minutes, seconds));
	}
	
	@Test
	public void whenSecondsEqualsZeroIsGivenProperTimeExpected() {
		int hours = 12;
		int minutes = 8;
		int seconds = 0;
		assertEquals(43680, ex.getTimeInSeconds(hours, minutes, seconds));
	}

}
