package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ProcessNumberTest {

	Excercise ex;

	@Before
	public void init() {
		ex = new Excercise();
	}

	@Test
	public void whenFirstArgOutOfRangeExeptionExpected() {
		int[] a = { -1, 256, -10, 123 };
		int b = 8;
		IllegalArgumentException iae = null;
		for (int num : a) {
			try {
				ex.processNumber(num, b);
			} catch (IllegalArgumentException e) {
				iae = e;
			}
			assertTrue(iae != null);
			iae = null;
		}
	}

	@Test
	public void whenSecondtArgOutOfRangeExeptionExpected() {
		int[] b = { -1, 256, -10, 423 };
		int a = 9;
		IllegalArgumentException iae = null;
		for (int num : b) {
			try {
				ex.processNumber(a, num);
			} catch (IllegalArgumentException e) {
				iae = e;
			}
			assertTrue(iae != null);
			iae = null;
		}
	}

	@Test(expected=IllegalArgumentException.class)
	public void whenArgInIncorrectOrderExeptionExpected() {
		int a = 3;
		int b = 1;
		ex.processNumber(a, b);
	}

	@Test
	public void whenLeftOddRightEven() {
		int a = 3 , b = 8;
		int[] expecteds = {4,6,8,7,5,3};
		assertArrayEquals(expecteds, ex.processNumber(a, b));
	}
	
	@Test
	public void whenLeftEvenRightEven() {
		int a = 2 , b = 8;
		int[] expecteds = {2,4,6,8,7,5,3};
		assertArrayEquals(expecteds, ex.processNumber(a, b));
	}
	
	@Test
	public void whenLeftEvenRightOdd() {
		int a = 2 , b = 7;
		int[] expecteds = {2,4,6,7,5,3};
		assertArrayEquals(expecteds, ex.processNumber(a, b));
	}
	
	@Test
	public void whenLeftOddRightOdd() {
		int a = 3 , b = 7;
		int[] expecteds = {4,6,7,5,3};
		assertArrayEquals(expecteds, ex.processNumber(a, b));
	}

	@Test
	public void whenArgEqualExpectedOneItemArray() {
		int a = 5 , b = 5;
		int[] expecteds = {5};
		assertArrayEquals(expecteds, ex.processNumber(a, b));
	}

}
