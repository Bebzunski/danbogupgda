package animals;

import java.util.LinkedList;
import java.util.List;

public class Animal{
	
	private List<String> animals = new LinkedList<>();

	public Animal() {
	}
	
	public void addAnimal(String animal, AnimalInterface ai){
		for(String anim : ai.make(animal)){
			animals.add(anim);
		}
		
	}
	public String getAnimal(int index){
		
		return (animals.size()>index) ? animals.get(index) : "";
	}
	public boolean containsAnimal(String animal, AnimalInterface ai){
		for(String anim : ai.make(animal)){
			if(!this.animals.contains(anim)){
				return false;
			}
		}
		return true;
	}
}
