package company;

public interface FilterInterface {
	
	public boolean test(String s);

}
