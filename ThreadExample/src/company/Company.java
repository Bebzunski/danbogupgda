package company;

import java.util.ArrayList;
import java.util.List;

public class Company {

	private List<Employee> employees = new ArrayList<>();

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public void filter(FilterInterface f, TransformInterface t) {
		for (Employee e : employees) {
			if(f.test(e.getName())){
				System.out.println(t.transform(e.getName()+" "+e.getLastname()));
			}
		}
			
	}
}
