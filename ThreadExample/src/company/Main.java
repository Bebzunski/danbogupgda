package company;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		
		Employee e1 = new Employee("Pawe�","Testowy",18,3000);
		Employee e2 = new Employee("Piotr","Przyk�adowy",32,10000);
		Employee e3 = new Employee("Julia","Doe",41,4300);
		Employee e4 = new Employee("Przemys�aw","Wietrak",56,4500);
		Employee e5 = new Employee("Zofia","Zaspa",37,3700);
		List<Employee> employees = new ArrayList<>();
		
		employees.add(e1); employees.add(e2); employees.add(e3); employees.add(e4); employees.add(e5);
		
		Company c = new Company();
		c.setEmployees(employees);
		
		c.filter((s) -> s.startsWith("P"), (s)-> s.toUpperCase());
		
	}

}
