package kalkulator;

@FunctionalInterface
public interface CalculatorInterface {
	
	public double doOperation(int a, int b);

}
