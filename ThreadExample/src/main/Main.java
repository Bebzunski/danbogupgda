package main;

import java.util.LinkedList;
import java.util.List;

import animals.Animal;
import animals.AnimalInterface;
import kalkulator.Calc;
import kalkulator.CalculatorInterface;

public class Main {

	public static void main(String[] args) {
		
		Thread t= new Thread(new PrzykladowyWatek());
		
		t.start();
		
		new Thread(new InnyWatek(), "jakis w�tek").start();
		System.out.println("KONIEC");
		new Thread(new Runnable() {
			public void run(){
				System.out.println("Pochodzee z kodu Runnable");
			}
		}).start();
		
		new Thread(() -> System.out.println("Wiadomosc z FI")).start();
		
		List<Person> p = new LinkedList<>();
		p.add(new Person(1));
		new Thread(() -> {
			int a = 1 , b = 3;
			System.out.println(a+b);
		}).start();
		
		Calc c = new Calc();
		
		System.out.println(c.oper(new CalculatorInterface() {
			
			@Override
			public double doOperation(int a, int b) {
				
				return a+b;
			}
		}, 2, 4));
		
		System.out.println(c.oper((a, b) -> (a-b), 7, 17));
		System.out.println(c.oper((a, b) -> (a*b), 7, 17));
		
		Animal a = new Animal();
		a.addAnimal("slon_kot_pies", new AnimalInterface() {
			
			@Override
			public String[] make(String anims) {
				String[] arrOfAnimals = anims.split("_");
				
				return arrOfAnimals;
			}
		});
		
		
		a.addAnimal("krokodyl|ma�pa|je�", (animalsy) -> animalsy.split("\\|"));
		
		
		a.addAnimal("katp", (animalsy) -> {
			String[] animal = animalsy.split("");
			String[] ret = new String[1];
			ret[0]="";
			for(int i = animalsy.length()-1 ; i>= 0 ; i--){
				ret[0]+=animal[i];
			}
			return ret;
		});
		
		System.out.println(a.containsAnimal("ptak slon kot pies krokodyl ma�pa je�", (animalsik) -> animalsik.split(" ")));

	}
}
