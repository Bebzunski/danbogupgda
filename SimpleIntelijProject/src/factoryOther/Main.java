package factoryOther;

import java.util.Random;


public class Main {

    public static void main(String[] args) {
        int[] numbers = new int[6];
        for(int i = 0 ; i < numbers.length ; i++){
            numbers[i] = new Random().nextInt(49)+1;
        }

        Coupon c = CouponFactory.getCoupon(numbers);
        System.out.println(c.getA());
    }
}
