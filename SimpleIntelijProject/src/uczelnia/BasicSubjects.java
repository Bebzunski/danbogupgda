package uczelnia;


public abstract class BasicSubjects {

    public void math(){
        System.out.println("Matematyka");

    }
    public void physics(){
        System.out.println("Fizyka");

    }
    public void subcjects(){
        math();
        physics();
        higherSubcjects();

    }

    public abstract void higherSubcjects();
}
