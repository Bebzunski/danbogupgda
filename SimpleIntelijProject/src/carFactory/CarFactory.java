package carFactory;


public class CarFactory {

    private CarFactory(){

    }

    public static Car getCar(CarEnum type){
    Car c = new Car(type.getName());

    return c;
    }
}
