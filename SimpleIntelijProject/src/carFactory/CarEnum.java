package carFactory;


public enum CarEnum {

    TRUCK("samochód ciężarowy"), CAR("samochód osobowy"), BUS("autobus"), TRACTOR("traktor");

    private String name;

    CarEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
