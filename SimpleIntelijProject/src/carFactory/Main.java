package carFactory;

import factory.AnimalFactory;

public class Main {

    public static void main(String[] args) {
        Car c1 = CarFactory.getCar(CarEnum.TRACTOR);
        System.out.println(c1.getType());

        Car c2 = CarFactory.getCar(CarEnum.CAR);
        System.out.println(c2.getType());
    }
}
