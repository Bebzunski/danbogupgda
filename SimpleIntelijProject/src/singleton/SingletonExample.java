package singleton;


public class SingletonExample {

    private static SingletonExample _instance;
    private String name;

    private SingletonExample() {

    }

    public static SingletonExample getInstance() {
        if (_instance == null) {
            System.out.println("Tworzę instancję");
            _instance = new SingletonExample();
        } else {
            System.out.println("Jest juz instancja ");
        }
        return _instance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
