package singleton;

public class Main {

    public static void main(String[] args) {
	    SingletonExample se1 = SingletonExample.getInstance();
	    se1.setName("Daniel");
        SingletonExample se2 = SingletonExample.getInstance();
        System.out.println(se2.getName());
    }
}
