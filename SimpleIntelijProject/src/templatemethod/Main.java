package templatemethod;


import singleton.SingletonExample;

public class Main {

    public static void main(String[] args) {

        Laptop laptop = new Laptop();
        System.out.println("Laptop:");
        laptop.externalDevice();

        MidiTowerComputer mtc = new MidiTowerComputer();
        System.out.println("MidiTowerComputer:");
        mtc.externalDevice();

        PersonalComputer pc = new PersonalComputer();
        System.out.println("PersonalComputer:");
        pc.devices();
    }
}
