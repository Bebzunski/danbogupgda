package templatemethod;


public abstract class BasicComputer {

    public void devices() {
        motherboard();
        procesor();
        externalDevice();
        System.out.println();
    }

    public void motherboard() {
        System.out.println("Motherboard");

    }
    public void procesor(){
        System.out.println("Procesor");
    }

    public abstract void externalDevice();
}
