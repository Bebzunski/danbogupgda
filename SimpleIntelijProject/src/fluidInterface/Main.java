package fluidInterface;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-06-22.
 */
public class Main {

    public static void main(String[] args) {

        List<Person> people = new LinkedList<>();

        people.add(new Person("Paweł","Testowy",30));
        people.add(new Person("Daniel","Testowy",23));
        people.add(new Person("Darek","Kowalski",14));
        people.add(new Person("Paweł","Nowak",40));

        People pp = new People();
        pp.addGroup("staff", people);
        for(Person person : pp.from("staff").name("Paweł").get()){
            System.out.println(person);
        }
    }
}
