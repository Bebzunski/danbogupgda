package fluidInterface;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class People {

    private HashMap<String, List<Person>> people = new HashMap<>();
    private List<Person> currentSearch;

    public People addGroup(String groupName, List<Person> ppl) {
        people.put(groupName, ppl);
        return this;
    }

    public People from(String from) {
        currentSearch = people.get(from);

        return this;
    }

    public People name(String name) {
        List<Person> list = new LinkedList<>();
        for (Person person : list) {
            if (person.getName().equals(name)) {
                list.add(person);
            }
        }
        currentSearch = list;
        return this;
    }

    public People lastname(String lastname) {

        List<Person> list = new LinkedList<>();
        for (Person person : list) {
            if (person.getLastname().equals(lastname)) {
                list.add(person);
            }
        }
        currentSearch = list;

        return this;
    }

    public List<Person> get() {
        return currentSearch;
    }

}
