package fluidInterface;


import java.util.LinkedList;
import java.util.List;

public class Beer {

    private String name;
    private String taste;
    private String type;
    private double price;

    public Beer(String name, String taste, String type, double price) {
        this.name = name;
        this.taste = taste;
        this.type = type;
        this.price = price;
    }

    public Beer() {
    }

    public String getName() {
        return name;
    }

    public Beer setName(String name) {
        this.name = name;
        return this;
    }

    public String getTaste() {
        return taste;
    }

    public Beer setTaste(String taste) {
        this.taste = taste;
        return this;
    }

    public String getType() {
        return type;
    }

    public Beer setType(String type) {
        this.type = type;
        return this;
    }

    public double getPrice() {
        return price;
    }

    public Beer setPrice(double price) {
        this.price = price;
        return this;
    }

    @Override
    public String toString() {
        return "Beer{" +
                "name='" + name + '\'' +
                ", taste='" + taste + '\'' +
                ", type='" + type + '\'' +
                ", price=" + price +
                '}';
    }


}
