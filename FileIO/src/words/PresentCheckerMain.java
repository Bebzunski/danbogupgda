package words;

public class PresentCheckerMain {

	public static void main(String[] args) {
		
		PresentChecker pc = new PresentChecker("words_zad9.txt");
		pc.readWords();
		for(String word : pc.wordsList){
			System.out.println(word);
		}
	}

}
