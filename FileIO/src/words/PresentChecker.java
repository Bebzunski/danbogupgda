package words;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class PresentChecker {
	
	private String filename;
	private final String path = "resources/";
	public ArrayList<String> wordsList = new ArrayList<>();

	public PresentChecker(String filename) {
		super();
		this.filename = filename;
	}

	public PresentChecker() {
		super();
	}
	
//	private boolean checkIfExists(String sentence){
//		
//		
//		return false;
//	}
	
	public void readWords(){
		
		File f = new File(path+filename);
		
		try{
			Scanner sc = new Scanner(f);
			while(sc.hasNextLine()){
				wordsList.add(sc.nextLine());
			}
			
			sc.close();
		} catch(FileNotFoundException e){
			System.out.println("ni ma");
		}
		
	}

}
