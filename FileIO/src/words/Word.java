package words;

public class Word {
	
	private String polishWord;
	private String englishWord;
	
	public String getPolishWord() {
		return polishWord;
	}

	public void setPolishWord(String polishWord) {
		this.polishWord = polishWord;
	}

	public String getEnglishWord() {
		return englishWord;
	}

	public void setEnglishWord(String englishWord) {
		this.englishWord = englishWord;
	}

	public Word(String polishWord, String englishWord) {
		super();
		this.polishWord = polishWord;
		this.englishWord = englishWord;
	}

	public Word() {
		super();
	}
	
	
	

}
