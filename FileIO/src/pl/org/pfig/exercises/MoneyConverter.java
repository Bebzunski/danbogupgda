package pl.org.pfig.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MoneyConverter {

	private final String filename = "resources/currency.txt";
	


	public double readCourse(String currency) {
		File f = new File(filename); // mozna podac r�cznie �cie�k�
		double ret = 0;

		try {
			String currentLine;
			Scanner sc = new Scanner(f);

			while (sc.hasNextLine()) {
				currentLine = sc.nextLine();
				String[] splitTab = currentLine.split("\t");
				String[] obcaWaluta = splitTab[0].split(" ");

				if (obcaWaluta[1].equalsIgnoreCase(currency)) {
					ret = Double.parseDouble(splitTab[1].replace(",", "."))/Double.parseDouble(obcaWaluta[0]);
					break;
				}
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println("nie ma pliku");
		}

		return ret;
	}
	
	public double convert(double money, String to){
		
		File f = new File(filename);
		double ret=0;
		
		try{
			Scanner sc = new Scanner(f);
			String currentLine;
			while(sc.hasNextLine()){
				
			currentLine = sc.nextLine();
			String[] temp = currentLine.split("\t");
			double exchangeToPLN = Double.parseDouble(temp[1].replace(",", ".")); 
			String[] value = temp[0].split(" ");
			if(to.equalsIgnoreCase(value[1])){
				
				ret = money*exchangeToPLN/Double.parseDouble(value[0]);
			break;
			}
			}
			sc.close();
		} catch(FileNotFoundException e){
			System.out.println("File not found :(");
		}
		
		return ret;
	}
	
	public double convert(double howMuchMoney, String from, String to){
		
		double ret = convert(howMuchMoney,from)/readCourse(to);
		
		
//		File f = new File(filename);
//		double ret = 0;
//		
//		try{
//			Scanner sc = new Scanner(f);
//			String currentLine ;
//		} catch (FileNotFoundException e){
//			System.out.println("nie ma pliku");
//		}
		
		return ret;
	}

}
