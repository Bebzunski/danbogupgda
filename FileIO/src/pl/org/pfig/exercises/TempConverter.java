package pl.org.pfig.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class TempConverter {
	
	private final String path = "resources/";
	
	public double toKelvin(double temp){
		
		return temp + 273.15;
	}
	
	public double toFahrenheit(double temp){
		
		return 9/5*temp + 32;
	}
	
	public double[] readTemp(String filename) {

		File f = new File(path + filename);
		double[] ret = new double[countLines(filename)];
		int i = 0;
		try {
			Scanner sc = new Scanner(f);
			String currentLine;
			
			while(sc.hasNextLine()){
			currentLine = sc.nextLine();
			ret[i++] = Double.parseDouble(currentLine.replace(",", "."));
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println("Ni ma pliku");
		}

		return ret;
	}

	public int countLines(String filename) {
		File f = new File(path+filename);
		int lines = 0;
		try{
			Scanner sc = new Scanner(f);
			while(sc.hasNextLine()){
				lines++;
				sc.nextLine();
			}
			sc.close();
		} catch (FileNotFoundException e){
			System.out.println("nie ma pliku");
		}
		return lines;
	}
	
	public void writeTemp(double[] tempInCal){
		
		File toK = new File(path+"tempK.txt");
		File toF = new File(path+"tempF.txt");
		
		try{
				FileOutputStream fosK = new FileOutputStream(toK);
				FileOutputStream fosF = new FileOutputStream(toF);
				
				PrintWriter pwK = new PrintWriter(fosK);
				PrintWriter pwF = new PrintWriter(fosF);
				
				for(double s : tempInCal){
					pwK.println(toKelvin(s));
					pwF.println(toFahrenheit(s));
				}
			pwK.close();
			pwF.close();
		} catch (FileNotFoundException e){
			System.out.println("nie ma plikuu");
		}
		
	}

}
