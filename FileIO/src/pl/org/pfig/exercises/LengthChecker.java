package pl.org.pfig.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class LengthChecker {
	
	private final String path = "resources/";
	
	public void make(String fileInput, int len){
		
		
		writeFile(readFile(fileInput),len);
		
	}
	
	private boolean isProperLength(String arg, int len){

		return (arg.length()>len);
	}
	
	private String[] readFile(String filename){
		
		File f = new File(path+filename);
		
		TempConverter tc = new TempConverter();
		String[] words = new String[tc.countLines(filename)];
		
		try{
			Scanner sc = new Scanner(f);
			
			for(int i = 0 ; i<words.length ; i++){
			words[i] = sc.nextLine();
			}
			sc.close();
		} catch (FileNotFoundException e){
			System.out.println("takiego pliku nie ma");
		}
		
		return words;
	}
	
	private void writeFile(String[] fileContent, int len){
		
		File f = new File(path+"words_"+len+".txt");
		try{ 
			FileOutputStream fos = new FileOutputStream(f);
			PrintWriter pw = new PrintWriter(fos);
			
			for(String word : fileContent){
				if(isProperLength(word, len))
				pw.println(word);
			}
			
			pw.close();
		} catch (FileNotFoundException e){
			System.out.println("nie ma pliku");
		}
	}

}
