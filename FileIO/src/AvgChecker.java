import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class AvgChecker {

	private String filename;
	private final String path = "resources/";

	public AvgChecker(String filename) {
		super();
		this.filename = filename;
	}

	public void process() {

		ArrayList<Double> listOfAvg = new ArrayList<>();
		ArrayList<String> listOfLines = new ArrayList<>();
		Double AvgOfAvgs = 0.0;
		File f = new File( path + filename);

		try {
			String currentLine;
			Scanner sc = new Scanner(f);

			while (sc.hasNextLine()) {
				currentLine = sc.nextLine();
				listOfLines.add(currentLine);
				String[] studentsNameAndMarks = currentLine.split("\t");

				double sum = 0.0;
				for (int i = 1; i < studentsNameAndMarks.length; i++) {
					sum += Double.parseDouble(studentsNameAndMarks[i]);
				}
				
				int numberOfMarks = studentsNameAndMarks.length - 1;
				AvgOfAvgs += sum/numberOfMarks;
				listOfAvg.add(sum/numberOfMarks);
			}

			AvgOfAvgs = AvgOfAvgs / listOfAvg.size();

			File f2 = new File(path+"new_" + filename);
			FileOutputStream fos = new FileOutputStream(f2);
			PrintWriter pw = new PrintWriter(fos);

			for (int i = 0; i < listOfAvg.size(); i++) {

				if (listOfAvg.get(i) > AvgOfAvgs) {
					pw.println(listOfLines.get(i));
				}
			}
			pw.close();
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println("nie ma pliku");
		}

	}

}
