package kolumny;

import org.junit.Test;

public class ColumnerTest {
	
	@Test
	public void sumColumnTest(){
		
		Columner c = new Columner("data.txt");
		assert c.sumColumn(2)<666.3;
		assert c.sumColumn(2)>666.2;
	}
	
	@Test
	public void avgColumnTest(){
		
		Columner c = new Columner("data.txt");
		assert c.avgColumn(2)<66.63;
		assert c.avgColumn(2)>66.62;
	}
	
	@Test
	public void maxColumnTest(){
		
		Columner c = new Columner("data.txt");
		assert c.maxColumn(3)==897.55;
		assert c.maxColumn(2)==123.1;
		assert c.maxColumn(1)==44;
	}
	
	@Test
	public void minColumnTest(){
		
		Columner c = new Columner("data.txt");
		assert c.minColumn(3)==5.345;
		assert c.minColumn(2)==12.123;
		assert c.minColumn(1)==1;
		assert c.minColumn(4)==0;
		
	}


}
