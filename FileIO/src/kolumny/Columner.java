package kolumny;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Scanner;

public class Columner {

	private String filename;
	private double[] currentColumn;
	private LinkedList<String> fileContent = new LinkedList<>();

	public Columner() {
		super();
	}

	public Columner(String filename) {
		super();
		this.filename = filename;
	}

	public double[] getCurrentColumn() {
		return currentColumn;
	}

	public void setCurrentColumn(double[] currentColumn) {
		this.currentColumn = currentColumn;
	}

	public double sumColumn(int column) {

		readColumn(column);
		double sum = 0;
		for (double num : this.currentColumn) {
			sum += num;
		}
		return sum;
	}

	public double avgColumn(int column) {

		return sumColumn(column) / currentColumn.length;
	}

	public int countColumn(int column) {
		readColumn(column);
		return fileContent.size();
	}

	public double maxColumn(int column) {
		readColumn(column);
		double max = currentColumn[0];
		for (int i = 1; i < currentColumn.length; i++) {
			if (max < currentColumn[i]) {
				max = currentColumn[i];
			}
		}
		return max;
	}

	public double minColumn(int column) {
		readColumn(column);
		double min = currentColumn[0];
		for (int i = 1; i < currentColumn.length; i++) {
			if (min > currentColumn[i]) {
				min = currentColumn[i];
			}
		}
		return min;
	}

	public void writeColumn(int column) throws ArrayIndexOutOfBoundsException{
		
		
		readColumn(column);
		if(column>fileContent.getFirst().split("\t").length){
			throw new ArrayIndexOutOfBoundsException();
		} else {
		File f = new File("resources/data_col_"+column);
		
		try{	
			FileOutputStream fos = new FileOutputStream(f);
			PrintWriter pw = new PrintWriter(fos);
			for(double num : currentColumn){
				pw.println(num);
			}
			pw.close();
		}	
		catch (FileNotFoundException e) {
			System.out.println("nie ma pliku");
		}
		}
	}

	private void readFile() {

		if (fileContent.size() == 0) {

			File f = new File("resources/" + filename);

			try {
				Scanner sc = new Scanner(f);
				while (sc.hasNext()) {
					fileContent.add(sc.nextLine());
				}
				sc.close();
			} catch (FileNotFoundException e) {
				System.out.println("nie ma pliku");
			}
		}

	}

	public void readColumn(int column) {

		readFile();
		// int maxNumOfCol = fileContent.getFirst().split("\t").length;
		currentColumn = new double[fileContent.size()];
		try {
			for (int i = 0; i < fileContent.size(); i++) {
				String[] arrOfNumbers = fileContent.get(i).split("\t");
				currentColumn[i] = Double.parseDouble(arrOfNumbers[column - 1].replace(",", "."));
			}
		} catch (ArrayIndexOutOfBoundsException e) {

		}
	}

}
