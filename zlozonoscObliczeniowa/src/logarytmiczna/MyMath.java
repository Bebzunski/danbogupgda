package logarytmiczna;

public class MyMath {

	public static void main(String[] args) {

		int[] data = { 1, 2, 6, 9, 11, 13, 23, 41, 43, 54, 90 };
		
		System.out.println(find(data, 6));

	}

	public static int find(int[] array, int value) {
		int a = 0;
		int b = array.length;

		while (a < b) {
			int s = (b + a) / 2;
			if (value <= array[s]) {
				b = s;
			} else {
				a = s;
			}
		}
		return array[a];
	}

}
