import org.junit.Test;

public class GeoMathTest {
	
	@Test
	public void rollerVolumeTest(){
		assert GeoMath.rollerVolume(2, 3)>37.699;
		assert GeoMath.rollerVolume(2, 3)<37.7;
	}
	
	@Test
	public void coneVolumeTest(){
		assert GeoMath.coneVolume(2, 3)>12.55;
		assert GeoMath.coneVolume(2, 3)<12.57;
	}
	
	@Test
	public void squarreAreaTest(){
		assert GeoMath.squareArea(4)==16;
		assert GeoMath.squareArea(0)==0;
		assert GeoMath.squareArea(1)==1;
	}
	@Test
	public void circleAreaTest(){
		assert GeoMath.circleArea(2)>12.56;
		assert GeoMath.circleArea(2)<12.57;
	}
	@Test
	public void piramideVolumeTest(){
		assert GeoMath.piramideVolume(2, 3)==4;
		assert GeoMath.piramideVolume(3, 4)==12;
		assert GeoMath.piramideVolume(8, 6)==128;
	}
	
	@Test
	public void cubeArea() {
		
		assert GeoMath.cubeArea(2)==24;
		assert GeoMath.cubeArea(3)==54;
		assert GeoMath.cubeArea(5)==150;
	}
	
	@Test
	public void cubeVolume(){
		assert GeoMath.cubeVolume(3)==27;
	}

}
