package pl.org.pfig.mydate;

public class MyDate {
	
	private int year  = 1;
	private int month = 1 ;
	private int day  = 1;
	private String[] strMonth = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
	private String[] strDays = {"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"};
	private int[] daysInMonths = {31,28,31,30,31,30,31,31,30,31,30,31};
	
	public MyDate(int year, int month, int day) {
		super();
		setDate(year, month, day);
	}
	
	
	
	public int getYear() {
		return year;
	}



	public int getMonth() {
		return month;
	}



	public int getDay() {
		return day;
	}



	public void setYear(int year) {
		if (year >= 1) {
			this.year = year;
		}
	}
	
	public void setMonth(int month){
		if(month>=1 && month<=12)
		this.month = month;
	}
	
	public void setDay(int day){
		if(day>=1 && day<=30)
		this.day = day;
	}
	
	public void setDate(int year,int month,int day){
		setYear(year);
		setMonth(month);
		setDay(day);
		
	}
	
	public String showMonth(int month){
		return strMonth[month-1];
	}
	
	@Override
	public String toString(){
		return day+" "+showMonth(month)+" "+year;
	}

}
