
public class GeoMath {

	public static void main(String[] args) {
		
		System.out.println(squareArea(13));
		System.out.println(cubeArea(2));
		System.out.println(circleArea(2));
		System.out.println(rollerVolume(2, 3));
		System.out.println(coneVolume(2, 3));
		

	System.out.println(piramideVolume(2,3));
	}

	public static double piramideVolume(double a, double h) {
		return a*a*h/3;
	}

	public static double squareArea(double a) {
			return a*a;
	}
	
	public static double cubeArea(double a) {
		return 6*squareArea(a);
	}
	
	public static double circleArea(double a){
		return a*a*Math.PI;
	}
	
	public static double rollerVolume(double a, double h){
		return a*a*h*Math.PI;
	}
	public static double coneVolume(double a,double h){
		return rollerVolume(a, h)/3;
	}

	public static double cubeVolume(double a) {
		return a*a*a;
	}
}
