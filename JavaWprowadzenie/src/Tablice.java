
public class Tablice {

	public static void main(String[] args) {
		
		int[] liczby = {2,3};
		System.out.println(liczby[0]);
		System.out.println(liczby[1]);
		
		int[] liczby100 = new int[100];
		System.out.println(liczby100[99]);
		
		boolean[] boole = new boolean[2];
		System.out.println(boole[1]);
		
		String[] slowa = new String[5];
		System.out.println(slowa[1]);
		
		char[] znaki = new char[3];
		System.out.println((int) znaki[1]);
		
		int[] tablica1 = {1,3,5,10};
		
		for(int i=0;i<tablica1.length;i++){
			System.out.println(tablica1[i]);
		}
		
		for(int i=tablica1.length-1;i>-1;i--){
			System.out.println(tablica1[i]);
		}
		
		// petla for each
		
		for(int liczba : tablica1){
			System.out.println(liczba);
		}
		
		
	}

}
