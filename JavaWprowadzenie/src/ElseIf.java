import java.util.Scanner;

public class ElseIf {

	
	public static void main(String[] args) {
		
		int a = 4;
		if(a%2==0) System.out.println(":)");
		else System.out.println(":(");

		Scanner ConsoleScanner = new Scanner(System.in);
		
		System.out.println("Podaj liczb�.");
		int i = ConsoleScanner.nextInt();
		if (i % 3 == 0 || i % 5 == 0)
			System.out.println("Liczba " + i + " jest podzielna przez 3 lub 5.");
		else
			System.out.println("Liczba " + i + " nie jest podzielna przez 3 oraz 5.");
		ConsoleScanner.close();
	}

}
