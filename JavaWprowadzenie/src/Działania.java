
public class Dzia�ania {

	public static void main(String[] args) {
		
		System.out.println("2+3");
		int a = 2+3;
		System.out.println(a);
		
		System.out.println();
		
		System.out.println("1-4");
		int b = 1-4;
		System.out.println(b);
		
		System.out.println();
		
		System.out.println("5/2");
		int c = 5/2;
		System.out.println(c);
		
		System.out.println();
		
		System.out.println("5/2.0");
		double d = 5/2.0;
		System.out.println(d);
		
		System.out.println();
		
		System.out.println("5.0/2");
		double e = 5.0/2;
		System.out.println(e);
		
		System.out.println();
		
		System.out.println("100L-10");
		long f = 100L-10;
		System.out.println(f);
		
		System.out.println();
		
		System.out.println("2f-3");
		float g = 2f-3;
		System.out.println(g);
		
		System.out.println();
		
		System.out.println("5f/2");
		float h = 5f/2;
		System.out.println(h);
		
		System.out.println();
		
		System.out.println("5d/2");
		double i = 5d/2;
		System.out.println(i);
		
		System.out.println();
		
		System.out.println("'A'+2");
		int j = 'A'+2;
		System.out.println(j);
		
		System.out.println();
		
		System.out.println("'a'+2");
		int k = 'a'+2;
		System.out.println(k);
		
		System.out.println();
		
		System.out.println("\"a\"+2");
		String l = "a"+2;
		System.out.println(l);

		System.out.println();
		
		System.out.println("\"a\"+\"b\"");
		String m = "a"+"b";
		System.out.println(m);
		
		System.out.println();
		
		System.out.println("'a'+'b'");
		int o = 'a'+'b';
		System.out.println(o);
		
		System.out.println();
		
		System.out.println("\"a\"+'b'");
		String p = "a"+'b';
		System.out.println(p);
		
		System.out.println();
		
		System.out.println("'a'+\"b\"");
		String r = "a"+'b';
		System.out.println(r);
		
		System.out.println();
		
		System.out.println("\"a\"+'b'+3");
		String s = "a"+'b'+3;
		System.out.println(s);
		
		System.out.println();
		
		System.out.println("'b'+3+\"a\"");
		String t = 'b'+3+"a";
		System.out.println(t);
		
		System.out.println();
		
		System.out.println("9%4");
		System.out.println(9%4);
	}

}
