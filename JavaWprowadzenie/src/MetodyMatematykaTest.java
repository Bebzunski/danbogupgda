import org.junit.Test;

public class MetodyMatematykaTest {

	// Publiczna, void, dowolna nazwa, prak parametrow
	@Test
	public void testIloczyn1(){
		
		assert metodyMatematyka.iloczyn(1, 1)==1;
		assert metodyMatematyka.iloczyn(0, 10)==0;
		assert metodyMatematyka.iloczyn(1, 10)==10;
		assert metodyMatematyka.iloczyn(4, 2)==8;
	
}	@Test
	public void porownujTest(){
		
		assert metodyMatematyka.najmniejszaLiczba(1,2,3)==1;
		assert metodyMatematyka.najmniejszaLiczba(1,1,1)==1;
		assert metodyMatematyka.najmniejszaLiczba(7,5,3)==3;
		assert metodyMatematyka.najmniejszaLiczba(10,3,5)==3;
	
}
	@Test
	public void porownujwTablicyTest(){
	int[] tablica1 = {25,8,11,90,13,55,6};
	int[] tablica2 = {25,50,11,90,13,55,60};
	int[] tablica3 = {25,80,19,90,13,55,60};
	int[] tablica4 = {25,80,110,90,130,55,60};
	
	assert metodyMatematyka.porownanieLiczb(tablica1)==6;
	assert metodyMatematyka.porownanieLiczb(tablica2)==11;
	assert metodyMatematyka.porownanieLiczb(tablica3)==13;
	assert metodyMatematyka.porownanieLiczb(tablica4)==25;

	}
	
}
