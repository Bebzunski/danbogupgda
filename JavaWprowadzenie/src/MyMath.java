
public class MyMath {
	public static void main(String[] args) {
		
		System.out.println(max(3, 4));
		System.out.println(max(3.0, 4.0));
		System.out.println(min(5, 7));
		System.out.println(min(5.0, 7.0));
		System.out.println(abs(-4));
		abs(-5);
		System.out.println(power(2.0,5));
		System.out.println(power(2,3));
	}

	public static int abs(int a) {
		if(a<0){
			return -a;
		} else {
			return a;
		}
	}
	
	public static double abs(double a) {
		if(a<0){
			return -a;
		} else {
			return a;
		}
	}

	public static long abs(long a) {
		if(a<0){
			return -a;
		} else {
			return a;
		}
	}

	public static int max(int a,int b) {
		if (a <= b) {
			return b;
		} else {
			return a;
		}
	}
	
	public static double max(double a,double b) {
		if (a <= b) {
			return b;
		} else {
			return a;
		}
	}
	
	public static int min(int a,int b) {
		if (a <= b) {
			return b;
		} else {
			return a;
		}
	}
	
	public static double min(double a,double b) {
		if (a <= b) {
			return b;
		} else {
			return a;
		}
	}
	
	public static double power(double a,int b) {
		double c = 1.0;
		for(int i=0;i<b;i++){
			c=c*a;
		}
		return c;
		
	}
	public static int power(int a,int b) {
		int c = 1;
		for(int i=0;i<b;i++){
			c=c*a;
		}
		return c;
		
	}

	public static float power(float a,int b) {
		float c = 1.0f;
		for(int i=0;i<b;i++){
			c=c*a;
		}
		return c;
		
	}
	
}
