
public enum UserSex {
	
	SEX_MALE("M�czyzna") , 
	SEX_FEMALE("Kobieta") , 
	SEX_OTHER("Inny");
	
	private String commonName;
	private int id;
	
	private UserSex(String sex){
		this.commonName = sex;
		
	
	}
	public String getCommonName(){
		return commonName;
	}
	
	public int getId(){
		return id;
	}

}
