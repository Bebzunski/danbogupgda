import org.junit.Test;

public class MyMathTest {
	
	@Test
	public void absIntTest(){
	assert MyMath.abs(-5)==5;
	assert MyMath.abs(0)==0;
	assert MyMath.abs(7)==7;
	}
	
	@Test
	public void absDoubleTest(){
	assert MyMath.abs(-5.0)==5;
	assert MyMath.abs(0.0)==0;
	assert MyMath.abs(7.0)==7;
	}
	
	@Test
	public void absLongTest(){
	long a = -5l;
	assert MyMath.abs(-5242l)==5242;
	assert MyMath.abs(1111l)==1111;
	assert MyMath.abs(a)==5;
	}
	
	@Test
	public void powerDoubleTest(){
		assert MyMath.power(3.25, 2)==10.5625;
		assert MyMath.power(2.5, 2)==6.25;
		assert MyMath.power(6.0, 3)==216;
	}
	
	@Test
	public void powerIntegerTest(){
		assert MyMath.power(3, 4)==81;
		assert MyMath.power(2, 4)==16;
		assert MyMath.power(6, 3)==216;
	}
	
	@Test
	public void powerFloatTest(){
		
		float a = 2.5f;
		assert MyMath.power(3f, 4)==81;
		assert MyMath.power(a, 2)==6.25;
		assert MyMath.power(6f, 3)==216;
	}
	
	@Test
	public void maxIntTest(){
		assert MyMath.max(5, 5)==5;
		assert MyMath.max(7, 5)==7;
		assert MyMath.max(-13, -15)==-13;

	}
	
	@Test
	public void maxDoubleTest(){
		assert MyMath.max(5.4, 5.0)==5.4;
		assert MyMath.max(7.4, 5.0)==7.4;
		assert MyMath.max(-13.0, -15.4)==-13;

	}

}
