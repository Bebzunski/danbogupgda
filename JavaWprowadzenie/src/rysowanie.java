
public class rysowanie {

	public static void main(String[] args) {

		rysujProstokat(3, 2);

		rysujTrojkatProstokatny(6);
		rysujPustaLinie(6);
		
		rysujPustyProstokat(10,6);
	}

	private static void rysujTrojkatProstokatny(int wysokosctr) {
		System.out.println();
		for (int i = 1; i <= wysokosctr; i++) {
			rysujLinie(i);
		}
	}

	private static void rysujProstokat(int dlugosc, int wysokosc) {
		System.out.println();
		for (int j = 1; j <= wysokosc; j++) {
			rysujLinie(dlugosc);
		}
		System.out.println();
	}

	private static void rysujLinie(int dlugoscLinii) {
		for (int i = 0; i < dlugoscLinii; i++) {
			System.out.print("*");
		}
		System.out.println();
	}
	
	private static void rysujPustaLinie(int dlugoscLinii) {
		System.out.print("*");
		for (int i = 2; i < dlugoscLinii; i++) {
			System.out.print(" ");
		}
		System.out.print("*");
		System.out.println();
	}
	
	private static void rysujPustyProstokat(int dlugosc,int wysokosc) {
		System.out.println();
		rysujLinie(dlugosc);
		for(int i = 2; i<wysokosc;i++){
			rysujPustaLinie(dlugosc);
		}
		rysujLinie(dlugosc);
		System.out.println();

	}
}