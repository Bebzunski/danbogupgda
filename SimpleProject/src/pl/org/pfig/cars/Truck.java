package pl.org.pfig.cars;

public class Truck extends Car{
	
	private String name;
	private int tires;
	private String engine;

	
	public Truck(String name, int tires, String engine) {
		super();
		this.name = name;
		this.tires = tires;
		this.engine = engine;
	}
	
	@Override
	public String getName() {
		return this.name;
	}
	@Override
	public int getTires() {
		return this.tires;
	}

}
