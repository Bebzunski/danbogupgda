package pl.org.pfig.data;

public class Llama implements AnimalInterface{

	private String name;
	
	public Llama(String name) {
		super();
		this.name = name;
	}

	@Override
	public String getName() {
		
		return this.name;
	}

}
