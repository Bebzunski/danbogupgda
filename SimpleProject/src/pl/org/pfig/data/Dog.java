package pl.org.pfig.data;

public class Dog  implements AnimalInterface, Soundable{
	
	private String name;

	public Dog(String name) {
		super();
		this.name = name;

	}

	public String getName(){
		return name;
	}
	
	@Override
	public String getSound(){
		return "ha� ha�";
	}

	@Override
	public String toString() {
		return "Dog : "+name;
	}
	
	public Dog newDog(String name){
		return new Dog(name);
	}

	
}
