package matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MatrixTransposeTest {

	private Matrix m;

	@Before
	public void init() {
		m = new Matrix();
	}

	@Test
	public void whenEmptyArrayGivenToMethodTranspose() {

		int[][] actual = new int[2][3];
		int[][] expected = new int[3][2];
		assertArrayEquals(expected, m.transpose(actual));
	}

	@Test
	public void whenValueArrayGivenToMethodTranspose() {

		int[][] actual = { { 2, 3, 4 }, { 1, 7, 9 } };
		int[][] expected = { { 2, 1 }, { 3, 7 }, { 4, 9 } };
		assertArrayEquals(expected, m.transpose(actual));
	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void test() {

		int[][] actual = { { 2, 3, 4 }, { 1, 7 } };
		m.transpose(actual);
	}

	@Test(expected = NullPointerException.class)
	public void test2() {

		int[][] actual = new int[3][];
		m.transpose(actual);
	}
	
	
}
