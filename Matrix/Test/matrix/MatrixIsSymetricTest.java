package matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MatrixIsSymetricTest {
	
	private Matrix m;
	
	@Before
	public void init(){
		m = new Matrix();
	}

	@Test
	public void whenEmptyArraysGivenToMethodIsSymetric() {
		int[][] squareArray = new int[4][4];
		int[][] notSquareArray = new int[3][4];
		assertEquals(true, m.isSymetric(squareArray));
		assertEquals(false, m.isSymetric(notSquareArray));
	}
	
	@Test
	public void whenArrayWithValuessGivenToMethodIsSymetric() {
		int[][] symetricArray = {{2,5},{5,6}};
		int[][] notSymetricArray = {{1,0},{1,0}};
		assertEquals(true, m.isSymetric(symetricArray));
		assertEquals(false, m.isSymetric(notSymetricArray));
	}
	
	

}
