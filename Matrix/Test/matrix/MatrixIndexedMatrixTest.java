package matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MatrixIndexedMatrixTest {
	
	private Matrix m;
	
	@Before
	public void init(){
		m = new Matrix();
	}

	@Test
	public void whenEmptyArrayGivenIndexedMatrixExpected() {
		int[][] actual = new int[2][3];
		int[][] expected = {{1,2,3},{4,5,6}};
		assertArrayEquals(expected, m.indexedMatrix(actual));
	}
	
	@Test
	public void whenArrayWithValuesGivenIndexedMatrixExpected() {
		int[][] actual = {{4,3,6},{1,8,-4}};
		int[][] expected = {{1,2,3},{4,5,6}};
		assertArrayEquals(expected, m.indexedMatrix(actual));
	}
}
