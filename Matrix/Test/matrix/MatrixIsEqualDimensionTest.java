package matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MatrixIsEqualDimensionTest {

	private Matrix m;
	
	@Before
	public void init(){
		m = new Matrix();
	}
	
	@Test
	public void whenTwoEmptyArraysIsEqualDimensionExpected(){
		int[][] actual1 = new int[1][4];
		int[][] actual2 = new int[1][4];
		assertEquals(true, m.isEqualDimension(actual1,actual2));
		
	}
	@Test
	public void whenTwoArraysWithValuesIsEqualDimensionExpected(){
		int[][] actual1 = {{2,6,1,-7},{1,-7},{-4,-6,-9,0}};
		int[][] actual2 = {{4,6,1,9},{5,9},{3,6,7,2}};
		assertEquals(true, m.isEqualDimension(actual1,actual2));
		
	}
	@Test
	public void whenFirstEmptySecondWithValuesArrayIsEqualDimensionExpected(){
		int[][] actual1 = new int [3][4];
		int[][] actual2 = {{4,6,1,9},{5,9,6,1},{3,6,7,2}};
		assertEquals(true, m.isEqualDimension(actual1,actual2));
		
	}
	@Test
	public void whenFirstEithValueSecondEmptyArrayIsEqualDimensionExpected(){
		
		int[][] actual1 = {{4,6,1,9},{5,9,6,1},{3,6,7,2}};
		int[][] actual2 = new int [3][4];
		assertEquals(true, m.isEqualDimension(actual1,actual2));
	}
}
