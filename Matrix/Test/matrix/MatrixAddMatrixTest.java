package matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MatrixAddMatrixTest {
	
	private Matrix m;
	
	@Before
	public void init(){
		m = new Matrix();
	}

	@Test(expected=IllegalArgumentException.class)
	public void whenDiffrentdDimensionsIllegalArgumentExceptionExpected() {
		int[][] actual1 = {{3,1,5},{2,6}};
		int[][] actual2 = {{3,1},{2,6}};
		
		m.addMatrix(actual1, actual2);
	}
}
