package matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MatrixIdentityMatrixMethodTest {
	
	private Matrix m;
	
	@Before
	public void init(){
		m = new Matrix();
	}
	
	@Test
	public void whenArrayGivenIdentityMatrixExpected() {
	int[][] actual = {{2,3},{5,7}} ;
	int[][] expected = 	{{1,3},{5,1}};
	assertArrayEquals(expected, m.identityMatrix(actual));
	
	}
	
	@Test
	public void whenEmptyArrayGivenIdentityMatrixExpected(){
		int[][] actuals = new int[3][3];
		int[][] expecteds = {{1,0,0},{0,1,0},{0,0,1}};
		assertArrayEquals(expecteds, m.identityMatrix(actuals));
	}
}