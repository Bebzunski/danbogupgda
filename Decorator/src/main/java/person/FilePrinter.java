package person;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

public class FilePrinter {

    private final PersonPrinter pp;

    public FilePrinter(PersonPrinter pp) {
        this.pp = pp;
    }

    public void printToFile(Person person, String filename){
        File f = new File("src/main/resources/"+filename);

        try {
            PrintStream ps = new PrintStream(f);
            pp.print(person,ps);
            ps.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
