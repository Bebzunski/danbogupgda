package person;

import java.io.PrintStream;

/**
 * Created by RENT on 2017-06-23.
 */
public class EyesColorPrinter implements PersonPrinter {

    private final PersonPrinter pp;

    public EyesColorPrinter(PersonPrinter pp) {
        this.pp = pp;
    }


    @Override
    public void print(Person person, PrintStream out) {
        pp.print(person, out);
        out.println("kolor oczu "+person.getEyesColour());
    }
}
