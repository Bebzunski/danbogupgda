package person;

import java.io.PrintStream;

public interface PersonPrinter {

    void print(Person person, PrintStream out);
}

