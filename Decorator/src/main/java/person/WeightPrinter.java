package person;

import java.io.PrintStream;


public class WeightPrinter implements PersonPrinter{

    private PersonPrinter pp;

    public WeightPrinter(PersonPrinter pp) {
        this.pp = pp;
    }

    @Override
    public void print(Person person, PrintStream out) {
        pp.print(person, out);
        out.println("waga " + person.getWeight());
    }
}
