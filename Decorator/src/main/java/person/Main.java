package person;


public class Main {

    public static void main(String[] args) {
        Person p = new Person("Daniel", "Bogdalski", 23, 75, 1.78, Person.EyesColour.BROWN);

        DataBuilder.getNameAndLastnameAnd().getAge().getWeight().asFilePrinter().printToFile(p , "data.txt");
    }
}