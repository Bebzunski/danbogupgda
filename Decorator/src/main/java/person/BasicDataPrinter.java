package person;

import java.io.PrintStream;

/**
 * Created by RENT on 2017-06-23.
 */
public class BasicDataPrinter implements PersonPrinter{

    public BasicDataPrinter(){

    }

    @Override
    public void print(Person person, PrintStream out) {
        out.println(person.getName()+" "+person.getSurname());
    }
}
