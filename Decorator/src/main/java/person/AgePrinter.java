package person;

import java.io.PrintStream;

/**
 * Created by RENT on 2017-06-23.
 */
public class AgePrinter implements PersonPrinter{

    private PersonPrinter pp;

    public AgePrinter(PersonPrinter pp) {
        this.pp = pp;
    }

    @Override
    public void print(Person person, PrintStream out) {
        pp.print(person, out);
        out.println("wiek: "+person.getAge());
    }
}
