package person;


public class Person {

    private String name;
    private String surname;
    private int age;
    private int weight;
    private double height;
    private EyesColour eyesColour;

    public Person(String name, String surname, int age, int weight, double height, EyesColour eyesColour) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.weight = weight;
        this.height = height;
        this.eyesColour = eyesColour;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public enum EyesColour {
        BROWN, GREEN;
    }

    public EyesColour getEyesColour() {
        return eyesColour;
    }

    public void setEyesColour(EyesColour eyesColour) {
        this.eyesColour = eyesColour;
    }
}
