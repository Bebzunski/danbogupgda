package person;

/**
 * Created by RENT on 2017-06-23.
 */
public class DataBuilder {

    private PersonPrinter pp = new BasicDataPrinter();

    public static DataBuilder getNameAndLastnameAnd(){
        return new DataBuilder();
    }

    public DataBuilder getAge(){
        pp = new AgePrinter(pp);
        return this;
    }

    public DataBuilder getWeight(){
        pp = new WeightPrinter(pp);
        return this;
    }

    public PersonPrinter asPrinter(){
        return pp;
    }

    public FilePrinter asFilePrinter(){

        return new FilePrinter(pp);
    }


}
