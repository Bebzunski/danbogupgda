package sportsmen;


public class HalfSeries implements Sportsman {

    private Sportsman sportsman;

    public HalfSeries(Sportsman sportsman) {
        this.sportsman = sportsman;
    }

    public void prepare() {

    }

    public void doPumps(int number) {
        sportsman.doPumps(number/2);
        System.out.println("Więcej mi się nie chce.");
    }

    public void doSquats(int number) {
        number = number/2;
        sportsman.doSquats(number);
        System.out.println("Więcej mi się nie chce.");

    }

    public void doCrunches(int number) {
        number = number/2;
        sportsman.doCrunches(number);
        System.out.println("Więcej mi się nie chce.");

    }
}
