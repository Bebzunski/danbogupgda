package sportsmen;


public class WaterDrinking implements Sportsman {

    private Sportsman sportsman;

    public WaterDrinking(Sportsman sportsman) {
        this.sportsman = sportsman;
    }

    public void prepare() {

        sportsman.prepare();
        System.out.println("Piję wodę");

    }

    public void doPumps(int number) {
        sportsman.doPumps(number);
        System.out.println("Piję wodę");

    }

    public void doSquats(int number) {
        sportsman.doSquats(number);
        System.out.println("Piję wodę");

    }

    public void doCrunches(int number) {
        sportsman.doCrunches(number);
        System.out.println("Piję wodę");

    }
}
