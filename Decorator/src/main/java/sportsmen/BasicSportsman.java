package sportsmen;

/**
 * Created by RENT on 2017-06-23.
 */
public class BasicSportsman implements Sportsman {

    public BasicSportsman() {
    }
    @Override
    public void prepare(){
        System.out.println("Przygotowuję się do ćwiczeń.");
    }

    @Override
    public void doPumps(int number){
        for(int i = 0 ; i < number ; i++){
            System.out.println("Robię pompkę !");
        }
    }

    @Override
    public void doSquats(int squats) {
        for (int i = 0; i < squats; i++)
            System.out.println("Robie przysiad nr " + (i + 1));
    }
    @Override
    public void doCrunches(int crunches) {
        for (int i = 1; i <= crunches; i++)
            System.out.println("Robie brzuszek nr " + i);
    }
}
