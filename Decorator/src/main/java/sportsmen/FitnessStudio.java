package sportsmen;


public class FitnessStudio {

    public void train(Sportsman sportsman){
        sportsman.prepare();
        sportsman.doPumps(2);
        sportsman.doSquats(5);
        sportsman.doCrunches(8);
    }

}
