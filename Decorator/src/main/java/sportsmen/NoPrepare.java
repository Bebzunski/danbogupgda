package sportsmen;

/**
 * Created by RENT on 2017-06-23.
 */
public class NoPrepare implements Sportsman {

    private Sportsman sportsman;

    public NoPrepare(Sportsman sportsman) {
    }

    public void prepare() {

    }

    public void doPumps(int number) {

    }

    public void doSquats(int number) {

    }

    public void doCrunches(int number) {

    }
}
