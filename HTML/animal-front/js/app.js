var app = angular.module('RESTApp', ['ngRoute']);
var url = 'http://localhost:8088/';

app.config(function($routeProvider) {
    var path = './views/';
    $routeProvider
        .when('/', {
            templateUrl: path + 'main.html'
        })
        .when('/add', {
            templateUrl: path + 'add.html',
            controller: 'addController'
        })
        .when('/show', {
            templateUrl: path + 'animals.html',
            controller: 'showController'
        })
        .when('/show/:id', {
            templateUrl: path + 'animal.html',
            controller: 'animalController'
        
        }).when('/spiecies', {
            templateUrl: path + 'spiecies.html',
            controller: 'spieciesController'
        
        }).when('/spiecie/:id', {
            templateUrl: path + 'spiecie.html',
            controller: 'spiecieController'
        
        }).when('/professions', {
            templateUrl: path + 'professions.html',
            controller: 'professionsController'
        
        }).when('/profession/:id', {
            templateUrl: path + 'profession.html',
            controller: 'professionController'
        
        }).when('/addStaff', {
            templateUrl: path + 'addStaff.html',
            controller: 'addStaffController'
        
        }).when('/showStaffs', {
            templateUrl: path + 'staffs.html',
            controller: 'showStaffsController'
        
        }).when('/showStaff/:id', {
            templateUrl: path + 'staff.html',
            controller: 'staffController'
        })
});

app.controller('addController', function($scope, $http) {
    
    $http({
        url: url + 'spiecies/show',
        dataType: 'json'
    }).then(function(success){
        $scope.spiecies = success.data;
    },function(error){
         console.error(error);
    });
    
    $scope.add = function() {
        $http({
            url: url + 'animals/add',
            method: 'GET',
            dataType: 'json',
            params: {
                
                name: $scope.animalName,
                description: $scope.desc,
                link: $scope.link,
                spiecie: $scope.spiecie
            }
        }).then(function(success) {
            console.log(success);
            $scope.message = "Dodano poprawnie zwierzątko.";
        }, function(error) {
            console.error(error);
        });
    }
});

app.controller('showController', function($scope, $http) {
    $http({
        url: url + 'animals/show',
        dataType: 'json'
    }).then(function(success) {
        var root = $('.animals');
        for(let i = 0; i < success.data.length; i++) {
            $('<li/>').html('<a href="/#!show/' + success.data[i].id + '">'+success.data[i].name+'</a>').appendTo(root);
        }
    }, function(error) {
        console.error(error);
    });
});

app.controller('animalController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'animals/show/' + id,
        dataType: 'json'
    }).then(function(success) {
        $scope.animal = success.data;
    }, function(error) {
        console.error(error);
    });
    
});

app.controller('spieciesController', function($scope, $http) {
    
    $http({
        url: url + 'spiecies/show'
    }).then(function(success) {
        $scope.spiecies = success.data;
    }, function(error) {
        console.error(error);
    });
    
    
    $scope.add = function() {
        $http({
            url: url + 'spiecies/add',
            dataType: 'json',
            params: {
                name: $scope.name,
                description: $scope.spiecieDesc
            }
        }).then(function(success) {
            if(success.data.id > 0) {
                $scope.spiecies.push(success.data);
                $scope.message = "Gatunek dodano poprawnie."
            } else
                $scope.message = "Wystąpił błąd podczas dodawania gatunku.";
        }, function(error) {
            console.error(error);
        });
    }
});

app.controller('spiecieController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'spiecies/show/' + id,
        dataType: 'json'
    }).then(function(success) {
        $scope.spiecie = success.data;
    }, function(error) {
        console.error(error);
    });
});


//controlery dla zawodów i użytkowników

app.controller('professionsController', function($scope, $http) {
    
    $http({
        url: url + 'professions/show'
    }).then(function(success) {
        $scope.professions = success.data;
    }, function(error) {
        console.error(error);
    });
    
    
    $scope.addProf = function() {
        $http({
            url: url + 'professions/add',
            dataType: 'json',
            params: {
                name: $scope.name,
                description: $scope.professionDesc
            }
        }).then(function(success) {
            if(success.data.id > 0) {
                $scope.professions.push(success.data);
                $scope.message = "Zawód dodano poprawnie."
            } else
                $scope.message = "Wystąpił błąd podczas dodawania zawodu.";
        }, function(error) {
            console.error(error);
        });
    }
});

app.controller('professionController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'professions/show/' + id,
        dataType: 'json'
    }).then(function(success) {
        $scope.profession = success.data;
    }, function(error) {
        console.error(error);
    });
});

app.controller('addStaffController', function($scope, $http) {
    
    $http({
        url: url + 'professions/show',
        dataType: 'json'
    }).then(function(success){
        $scope.professions = success.data;
    },function(error){
         console.error(error);
    });
    
    $scope.addStaff = function() {
        $http({
            url: url + 'staffs/add',
            method: 'GET',
            dataType: 'json',
            params: {
                
                name: $scope.staffName,
                lastname: $scope.staffLastname,
                salary: $scope.staffSalary,
                profession: $scope.profession
                
            }
        }).then(function(success) {
            console.log(success);
            
                $scope.staffName = "";
                $scope.staffLastname = "";
                $scope.staffSalary = "";
                $scope.profession = "";
            
            $scope.message = "Dodano poprawnie użytkownika.";
        }, function(error) {
            console.error(error);
        });
    }
});

app.controller('showStaffsController', function($scope, $http) {
    $http({
        url: url + 'staffs/show',
        dataType: 'json'
    }).then(function(success) {
        var root = $('.staffs');
        for(let i = 0; i < success.data.length; i++) {
            $('<li/>').html('<a href="/#!showStaff/' + success.data[i].id + '">'+success.data[i].name+' '
                            + success.data[i].lastname+'</a>').appendTo(root);
        }
    }, function(error) {
        console.error(error);
    });
});

app.controller('staffController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'staffs/show/' + id,
        dataType: 'json'
    }).then(function(success) {
        $scope.staff = success.data;
    }, function(error) {
        console.error(error);
    });
    
});


    
    
    
    
    
    
