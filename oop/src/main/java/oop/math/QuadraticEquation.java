package oop.math;

public class QuadraticEquation {
	
	private double a;
	private double b;
	private double c;
	
	public QuadraticEquation(double a, double b, double c) {
		super(); // nie musi tego by�
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	public void showEquation(){
		System.out.println(a+"*X^2 + "+b+"*X + "+c+" = 0");
	}
	public double calcDelta(){
		return Math.pow(b, 2)-4*a*c;
	}
	
	public double calcX1(){
		return (-b-Math.sqrt(calcDelta()))/(2*a);
	}
	
	public double calcX2(){
		return (-b+Math.sqrt(calcDelta()))/(2*a);
	}
}
