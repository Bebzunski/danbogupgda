package oop.osoba;

public class TeddyBear {
	
	private String name;
	private double height;
	private int mass;
	
	public TeddyBear(String name){
		this.name = name;
	}
	
	public TeddyBear(String name, double height, int mass){
		this.name = name ;
		this.height = height;
		this.mass = mass;
	}
	
	public void showName(){
		System.out.println(name);
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public double getHeight() {
		return height;
	}
	
	public void setHeight(double height) {
		this.height = height;
	}
	
	public int getMass() {
		return mass;
	}
	public void setMass(int mass) {
		this.mass = mass;
	}

}
