package oop.osoba;

public class OsobaMain {

	public static void main(String[] args) {
		Osoba jan = new Osoba("Jan",23); // Tworzenie instancji 
		
		Osoba adam = new Osoba("Adam",23);
		
		jan.przedstawSie();
		adam.setImie("Jacek");
		adam.przedstawSie();
		
		System.out.println("Imie adama to : "+adam.getImie());
		
		TeddyBear misio = new TeddyBear("Daniel");
		TeddyBear misio2 = new TeddyBear("Dariusz",2.56,250);
		misio.showName();
		
		System.out.println(misio.getName());
		System.out.println(misio2.getName()+" "+misio2.getHeight()+"m "+misio2.getMass()+"kg");
	}

}
