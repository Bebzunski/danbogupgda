package oop;

public class MutableDouble {
	
	private double value;
	
	
	
	public MutableDouble() {
		super();
	}

	public MutableDouble(double value){
		this.value = value;
	}
	
	public void setValue(double value){
		this.value=value;
	}

	public double getValue(){
		return value;
	}
	
	public void increment(){
		value++;
	}
}
