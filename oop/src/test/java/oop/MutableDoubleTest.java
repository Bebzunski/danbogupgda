package oop;

import org.junit.Test;

public class MutableDoubleTest {
	
	@Test
	public void test(){
		
		MutableDouble a = new MutableDouble(2.5);
		MutableDouble b = a;
		
		a.setValue(5.5);
		
		assert a.getValue() == 5.5;
		assert b.getValue() == 5.5;
	}

}
