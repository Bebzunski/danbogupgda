package pl.org.pfig.students.model;

public class Student {

    private String name;
    private String last;

    public Student(String name, String last) {
        this.name = name;
        this.last = last;
    }

    public Student() {
    }

    public String getName() {
        return name;
    }

    public Student setName(String name) {
        this.name = name;
        return this;
    }

    public String getLast() {
        return last;
    }

    public Student setLast(String last) {
        this.last = last;
        return this;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", last='" + last + '\'' +
                '}';
    }
}
