package pl.org.pfig.students;

import pl.org.pfig.students.resolver.StudentResolver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Main {

    public static void main(String[] args) {

        StudentResolver sr = new StudentResolver();

        System.out.println(sr.get(1));
//        System.out.println(sr.delete(2));

        List<String> student = new ArrayList<>();
        Map<String, String> studentMap = new HashMap<>();
        student.add("id");
        student.add("'Franek'");
        student.add("'Szyszko'");

        System.out.println(sr.insertAll(student));
        System.out.println(sr.delete(2));

    }
}
