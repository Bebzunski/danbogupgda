package pl.org.pfig.students.resolver;

import pl.org.pfig.cities.connector.*;
import pl.org.pfig.cities.model.City;
import pl.org.pfig.students.model.Student;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;


public class StudentResolver extends AbstractResolver<Student> {

    @Override
    public Student get(int id) {
        Student ret = null;
        try {
            Connection connection = DatabaseConnector.getInstance().getConnection();
            String query = "SELECT * FROM `student` where `id` = " + id+";";

            Statement s = connection.createStatement();
            ResultSet rs = s.executeQuery(query);

            if(rs.next()) {
                ret = new Student(rs.getString("name"), rs.getString("lastname"));
            }
            rs.close();
            s.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return ret;
    }

    @Override
    public boolean delete(int id) {
        boolean ret = false;
        try{
            Connection c = DatabaseConnector.getInstance().getConnection();
            Statement s = c.createStatement();

            String query = "DELETE FROM `student` WHERE `id` = " + id ;

            s.execute(query);
            s.close();

            ret = true;
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return ret;
    }

    @Override
    public boolean insert(Map<String, String> params) {
            boolean ret = false;
        try {
            Connection connection = DatabaseConnector.getInstance().getConnection();
            String query = "Insert INTO `student` Values (";
                    for(String key : params.keySet()){
                        query = query + params.get(key)+",";
                    }
                    char last = query.charAt(query.length()-1);
                    query.replace(last , ')');

            Statement s = connection.createStatement();
            if(s.executeUpdate(query) > 0) {
                ret = true;
            }

            s.close();

            ret = true;

        } catch (SQLException e) {
        }
        return ret;
    }

    public boolean insertAll(List<String> params) {
        boolean ret = false;
        try {
            Connection connection = DatabaseConnector.getInstance().getConnection();
            String query = "Insert INTO `student` Values (";
            for(String value : params){
                query = query + value+",";
            }
            char last = query.charAt(query.length()-1);
            query = query.substring(0 , query.length()-1)+");";

            Statement s = connection.createStatement();

            if(s.executeUpdate(query) > 0) {
                ret = true;
            }
            s.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return ret;
    }

    @Override
    public boolean update(int id, Map<String, String> params) {

        boolean ret = false;

        try{
            Connection connection = DatabaseConnector.getInstance().getConnection();
            String query = "UPDATE `student` SET ";
            for(String key : params.keySet()){
                query = query + key+" = "+params.get(key)+",";
            }
            char last = query.charAt(query.length()-1);
            query.replace(last , ' ');
            query = query + "WHERE 'id' = "+id;

            Statement s = connection.createStatement();
            ResultSet rs = s.executeQuery(query);

            rs.close();
            s.close();
            connection.close();

            ret = true;
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }

        return ret;
    }
}
