package pl.org.pfig.students.resolver;

import java.util.Map;


public abstract class AbstractResolver<T> {

    public abstract T get(int id);

    public abstract boolean delete(int id);

    public abstract boolean insert(Map<String, String> params);

    public abstract boolean update(int id, Map<String, String> params);

}
