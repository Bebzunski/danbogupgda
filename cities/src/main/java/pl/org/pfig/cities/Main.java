package pl.org.pfig.cities;


import pl.org.pfig.cities.connector.DatabaseConnector;
import pl.org.pfig.cities.model.City;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        try{
            Connection c = DatabaseConnector.getInstance().getConnection();
            Statement s = c.createStatement();
            s.execute("UPDATE `city` SET `name` = 'Wawa' WHERE `name` LIKE 'warszawa'");
            s.close();
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }

        try{
            Connection c = DatabaseConnector.getInstance().getConnection();
            Statement s = c.createStatement();
            s.execute("DELETE FROM `city` WHERE `name` LIKE 'Wronki'");
            s.close();
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }

        try{
            Connection c = DatabaseConnector.getInstance().getConnection();
            Statement s = c.createStatement();
            s.execute("INSERT INTO `city` VALUES(NULL, 'Wronki')");
            s.close();
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }


        //select

        List<City> listOfCities = new ArrayList<>();
        try {
            Connection connection = DatabaseConnector.getInstance().getConnection();
            String query = "SELECT * FROM `city`";

            Statement s = connection.createStatement();
            ResultSet rs = s.executeQuery(query);

            while(rs.next()){
//                System.out.println(rs.getInt("id")+". " + rs.getString("name"));
                listOfCities.add(new City(rs.getInt("id"), rs.getString("name")));
            }

            rs.close();
            s.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        for(City c : listOfCities){
            System.out.println("> " + c.getCity());
        }

    }
}
