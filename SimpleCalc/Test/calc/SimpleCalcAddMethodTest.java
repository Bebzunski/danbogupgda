package calc;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SimpleCalcAddMethodTest {
	
	private SimpleCalc sc;
	
	@Before
	public void doStuff(){
		sc = new SimpleCalc();
	}

	@Test
	public void whenTwoPositiveNumbersAreGivenPositiveNumberAreSum() {
		int a = 3;
		int b = 6;
//		sc.add(a, b);
		assertEquals(9, sc.add(a, b));
		assert sc.add(a, b) == 9;
	}
	
	@Test
	public void whenTwoNegativeNumbersAreGivenNegativeNumberAreSum(){
		int a = -3;
		int b = -4;
		assertEquals(-7, sc.add(a, b));
	}
	@Test
	public void whenFirstPositiveSecondNegativeNumberGiven(){
		int a= 6;
		int b= -4;
		assertEquals(2, sc.add(a, b));
	}
	@Test
	public void whenFirstNegativeSecondPostive(){
		int a = -6;
		int b = 3;
		assertEquals(-3, sc.add(a, b));
	}
	
	@Test(expected=Exception.class)
	public void whenExThrowMethodIaUsedExceptionIsThrown() throws Exception{

		sc.exThrow();
	}
	
	@Test
	public void whenTwoMaxIntegerAreGivenPositiveNumberAreExpected(){
		assertTrue("Out of range", sc.add(Integer.MAX_VALUE,Integer.MAX_VALUE+3)>0);
	}
}
