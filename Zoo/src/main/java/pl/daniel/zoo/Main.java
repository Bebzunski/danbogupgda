package pl.daniel.zoo;

import org.hibernate.*;
import org.hibernate.query.Query;
import org.hibernate.cfg.Configuration;
import pl.daniel.zoo.entity.Animal;

import javax.persistence.metamodel.EntityType;

import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        Session session = new Configuration().configure().buildSessionFactory().openSession();

        Transaction t = session.beginTransaction();
        Animal a = new Animal();
        a.setName("Słoń");
        session.save(a);
        session.save(new Animal("Żyrafa"));
        session.save(new Animal("Pies"));
        t.commit();

        Animal animal = null;
        t = session.beginTransaction();
        String query = "from Animal where id = :id";
        Query q = session.createQuery(query);
        q.setParameter("id", 1);
        animal = (Animal) q.uniqueResult();
        System.out.println(animal.getId() + " | " + animal.getName());
        t.commit();

        t = session.beginTransaction();
        animal.setName("Krokodyl");
        session.update(animal);
        t.commit();

        t = session.beginTransaction();
        Animal animalToDelete = session.load(Animal.class, new Integer(1));
        session.delete(animalToDelete);
        t.commit();

        List<Animal> animals = session.createQuery("from Animal").list();
        for(Animal anim : animals){
            System.out.println(anim.getId()+" "+ anim.getName());
        }
        session.close();
    }
}