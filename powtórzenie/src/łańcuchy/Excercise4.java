package �a�cuchy;

public class Excercise4 {

	public static void main(String[] args) {

		System.out.println(flipString("dANi3L boGDaLSKi"));

	}

	public static String flipString(String word) {

		String flipedWord = "";
		for (int i = 0; i < word.length(); i++) {
			flipedWord = flipedWord + flipLetter(word.charAt(i));
		}
		return flipedWord;
	}

	public static char flipLetter(char letter) {

		if (Character.isUpperCase(letter)) {
			letter = Character.toLowerCase(letter);
		} else if (Character.isLowerCase(letter)) {
			letter = Character.toUpperCase(letter);
		}
		return letter;
	}
}
