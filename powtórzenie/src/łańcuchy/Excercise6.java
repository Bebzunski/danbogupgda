package �a�cuchy;

public class Excercise6 {

	public static void main(String[] args) {

		System.out.println(strToInt("+12"));
		System.out.println(strToInt("0001"));
		System.out.println(strToInt("991-234-23"));
		System.out.println(strToInt("+zonk"));
		System.out.println(strToInt(""));
		System.out.println(strToInt("-12e5"));
		System.out.println(strToInt("-12e-5"));

	}

	public static int strToInt(String str) {
		int ret = 0;
		int index = 0;
		
		if (str.length() == 0) {
			return ret;
		}

		int sing = 1;
		if (str.charAt(0) == '-') {
			sing = -1;
			index++;
		} else if (str.charAt(0) == '+') {
			index++;
		}

		while (str.charAt(index) == '0') {
			index++;
		}

		String number = "";
		while (index < str.length() && Character.isDigit(str.charAt(index))) {
			number = number + str.charAt(index);
			index++;
		}

		if (number.length() > 0) {
			ret = sing * Integer.parseInt(number);
			return ret;
		}
		return ret;
	}

}
