package �a�cuchy;

public class ExceriseTest {

	public static void main(String[] args) {
		
		System.out.println(modify("AFD209UJKL7"));

	}

	public static String modify(String str) {

		int count = 0;
		String ret = "";
		int number;

		for (int i = 0; i < str.length(); i++) {

			char current = str.charAt(i);		

			if (Character.isDigit(current)) {
				number = Integer.parseInt("" + current);
				if (number >= 2 && number <= 9) {
					number -= 2;
				}
				ret = ret + number;
			} else {
				if (count % 2 == 1) {
					ret = ret + current;
				}
				count++;
			}
		}

		return ret;
	}
}
