package trudne;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Main {

	public static void main(String[] args) {

		Random r = new Random();
		int[] randomArray = new int[100];
		for (int i = 0; i < randomArray.length; i++) {
			randomArray[i] = (r.nextInt(9) + 1);
		}

		HashMap<Integer, Integer> map = countAllNumbers(randomArray);

		for(Map.Entry<Integer, Integer> entry : map.entrySet()){
			System.out.println("Liczba "+entry.getKey()+" wyst�puje "+entry.getValue()+" razy.");
		}
	}

	public static List<Double> allMultiple(double smaller, double bigger) {

		List<Double> list = new ArrayList<>();
		if (smaller > 0 && bigger > 0) {
			double multiple = smaller;
			while (multiple < bigger) {
				list.add(multiple);
				multiple += smaller;
			}
		}
		return list;
	}

	public static HashMap<Integer, Integer> countAllNumbers(int[] arr) {

		HashMap<Integer, Integer> map = new HashMap<>();
		for (int i = 0; i < arr.length; i++) {
			if (map.containsKey(arr[i]) == false) {
				map.put(arr[i], 1);
			} else {
				map.put(arr[i], map.get(arr[i]) + 1);
			}
		}
		return map;
	}
}
