package sredniPoziom;

import java.util.function.Function;

public class PersonMain {

	public static void main(String[] args) {

		System.out.println(integral(100000, (x) -> x * x));
		System.out.println(integral(100000, (x) -> 2 * x + Math.sqrt(x)));

		System.out.println(integral(10000, PersonMain::myFunction));
		System.out.println(checkIfPrimeNumber(1353325439));

	}

	public static double integral(int partition, Function<Double, Double> function) {
		double field = 0;
		double step = 10.0 / partition;
		for (int i = 0; i < partition; i++) {
			field += step * (function.apply(i * step) + function.apply((i + 1) * step)) / 2;
		}
		return field;
	}

	private static double myFunction(double x) {
		return 2 * x + Math.sqrt(x);
	}

	public static boolean checkIfPrimeNumber(double n) {
		if (n % 2 == 0) {
			return true;
		} else {
			for (double i = 3; i * i < n; i+=2) {
				if (n % i == 0) {
					System.out.println(i);
					return true;
				}
			}
			return false;
		}
	}

}
