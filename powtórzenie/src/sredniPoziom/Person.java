package sredniPoziom;

import java.util.Random;

public class Person {
	
	private String name;
	private String secondName;
	
	static Random r = new Random();
	
	
	public Person(String name, String secondName) {
		super();
		this.name = name;
		this.secondName = secondName;
	}



	public static Person randomPerson(){
		
		
		return new Person(
				rand("Daniel", "Karol","Katarzyna","Anna", "Jacek", "Karolina", "Adam"), 
				rand("Kowalski", "Nowak", "Lewandowski", "Wi�niewski"));
	}



	public static String rand(String... names) {
		int i = r.nextInt(names.length);
		String rname = names[i];
		return rname;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getSecondName() {
		return secondName;
	}



	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	@Override
	public String toString() {
		return getName()+" "+getSecondName();
	}
	
	
}
