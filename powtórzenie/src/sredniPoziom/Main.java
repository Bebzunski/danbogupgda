package sredniPoziom;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

	public static void main(String[] args) {
		
		double[] arr2 = {5,3.6,4,1.5};
		
		double[] arr3 = allSquare(array(3,5,7,8));
		
		for(double num: arr3){
			System.out.print(num+" ");
		}
		
		System.exit(0);
		
		
		List<Double> list2 = new ArrayList<>();
		list2.add(2.3);
		list2.add(1.3);
		list2.add(1.1);
		
		double[] newArr = addToArray(list2, arr2);
		
		for(double num: newArr){
			System.out.print(num+" ");
		}
		
		
		
		
		
		double[] arr = {5,3.6,4,1.5};
		List<Double> list = copyToList(arr);
		list.add(44.44);
		copyToList(arr, list);
		for(double num : list){
			System.out.print(num+"  ");
		}
		


		Random r = new Random();
		double[] randomArray = new double[r.nextInt(19) + 1];
		for (int i = 0; i < randomArray.length; i++) {
			randomArray[i] = (r.nextInt(9) + 1);
		}

		for (double num : randomArray) {
			System.out.print(num + " ");
		}
		System.out.println();
		double[] copy = copyArray(randomArray);
		for(double element : copy){
			System.out.print(element+" ");
		}
		

		allMultiple(3, 78);
		
		
		System.out.println();
		System.out.println("Ilo�� wyst�pie� liczby '1' = " + countNumber(randomArray, 7));

	}

	public static int countNumber(double[] array, int num) {

		int count = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] == num) {
				count++;
			}
		}
		return count;
	}

	public static void allMultiple(int smaller, int bigger) {
		if (smaller > 0 && bigger > 0) {
			int multiple = smaller;
			while (multiple <= bigger) {
				System.out.print(multiple + " ");
				multiple += smaller;
			}
		} else {
			System.out.println("Poda�es nieporpawne liczby.");
		}

	}

	public static double[] copyArray(double[] arr) {
		double[] copyOfArray = new double[arr.length];
		for (int i = 0; i < arr.length; i++) {
			copyOfArray[i] = arr[i];
		}
		return copyOfArray;
	}
	
	public static List<Double> copyToList(double[] arr){
		List<Double> list = new ArrayList<Double>();
		for(double element : arr){
			list.add(element);
		}
		return list;
	}
	
	public static void copyToList(double[] arr, List<Double> list){
		for(double element : arr){
			list.add(element);
		}
	}
	
	public static double[] addToArray(List<Double> list, double[] arr){
		double[] newArr = new double[arr.length+list.size()];
		
		for(int i=0 ; i<arr.length ; i++){
			newArr[i]=arr[i];
		}
		for(int j=arr.length ; j<(arr.length+list.size()) ; j++){
			newArr[j]=list.get(j-arr.length);
		}
		return newArr;
	}
	
	
	public static double[] array(double... arr){
		return arr;
	}
		
	public static double[] allSquare(double[] arr){
		
//		double[] newArr = new double[arr.length];
		for(int i=0 ; i<arr.length ; i++){
			arr[i] = arr[i]*arr[i];
		}
		return arr;

	}
	
}
