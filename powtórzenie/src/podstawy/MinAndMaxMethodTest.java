package podstawy;

import java.util.Scanner;

public class MinAndMaxMethodTest {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		minAndMaxInput(sc);
		sc.close();

	}

	public static void minAndMaxInput(Scanner sc) {

		System.out.println("Podaj po spacji dowoln� ilo�� liczb ca�kowitych\nnp. \"4 -2 6 7 -9\"");
		try {
			String[] input = sc.nextLine().split(" ");
			int[] numbers = parseStringsToIntegers(input);
			minAndMax(numbers);
		} catch (NumberFormatException e) {
			minAndMaxInput(sc);
		}

	}

	public static void minAndMax(int... numbers) {

		int min = minimum(numbers);
		int max = maximum(numbers);

		System.out.println("Najmniejsza liczba : " + min);
		System.out.println("Najwi�ksza liczba : " + max);
	}

	public static int[] parseStringsToIntegers(String[] strings) {
		int[] numbers = new int[strings.length];

		for (int i = 0; i < strings.length; i++) {
			numbers[i] = Integer.parseInt(strings[i]);
		}
		return numbers;
	}

	public static int maximum(int a, int b) {
		if (a > b) {
			return a;
		} else {
			return b;
		}
	}

	public static int minimum(int a, int b) {
		if (a > b) {
			return b;
		} else {
			return a;
		}
	}

	public static int maximum(int... numbers) {
		int max = numbers[0];
		for (int i = 1; i < numbers.length; i++) {
			max = maximum(max, numbers[i]);
		}
		return max;
	}

	public static int minimum(int... numbers) {
		int min = numbers[0];
		for (int i = 1; i < numbers.length; i++) {
			min = minimum(min, numbers[i]);
		}
		return min;
	}

}
