package podstawy;

import java.util.InputMismatchException;

public class RomanNumerals {

	static String[] roman1 = { "I", "X", "C", "M" };
	static String[] roman5 = { "V", "L", "D" };

	public static void main(String[] args) {

		System.out.println(toRomanNumerals(2543));

	}
	
	// zamiana inta na liczb� rzymsk�
	 

	public static String toRomanNumerals(int number) {

		if (number < 0 || number > 3999) {
			throw new InputMismatchException();
		}

		int[] digits = splitDigits(number);
		digits = changeOrder(digits);
		String roman = "";
		for (int i = 0; i < digits.length; i++) {
			roman = createRomanNumber(digits[i], i) + roman;
		}
		return roman;
	}
	
	public static int[] splitDigits(int number){
		
		String[] split = ("" + number).split("");
		return parseToIntegers(split);
	}

	public static String createRomanNumber(int digit, int size) {

		if (digit == 9) {
			return roman1[size] + roman1[size + 1];
		}

		if (digit == 4) {
			return roman1[size] + roman5[size];
		}

		String roman = "";

		if (digit >= 5 && digit <= 8) {
			roman = roman5[size];
		}

		if (digit % 5 >= 1 && digit % 5 <= 3) {
			for (int i = 0; i < digit % 5; i++) {
				roman = roman + roman1[size];
			}
		}

		return roman;
	}

	public static int[] changeOrder(int[] arr) {
		int[] copy = new int[arr.length];
		for (int i = 0; i < arr.length; i++) {
			copy[i] = arr[arr.length - 1 - i];
		}
		return copy;
	}

	public static int[] parseToIntegers(String[] array) {
		int[] digits = new int[array.length];
		for (int i = 0; i < array.length; i++) {
			digits[i] = Integer.parseInt(array[i]);
		}
		return digits;
	}

	// zamiana rzymskiej na inta
	
	
	
}
