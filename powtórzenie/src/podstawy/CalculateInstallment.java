package podstawy;

public class CalculateInstallment {

	public static void main(String[] args) {

		System.out.println(getInstallment(1000, 8));
		System.out.println(getInstallment(1000, 15));
		System.out.println(getInstallment(1000, 35));

	}

	public static double getInstallment(int price, int nrOfInstallments) {

		double interest = 1 + setInterest(nrOfInstallments);
		double newPrice = interest * price;
		double installment = newPrice / nrOfInstallments * 100;
		installment = Math.round(installment)/100.0;
		return installment;
	}

	public static double setInterest(int nrOfInstallments) {
		double interest = 0;
		if (nrOfInstallments >= 6 && nrOfInstallments <= 12) {
			interest = 0.025;
		} else if (nrOfInstallments <= 24) {
			interest = 0.05;
		} else if (nrOfInstallments <= 48) {
			interest = 0.1;
		}
		return interest;
	}
}
