package podstawy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ExerciseNr9 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		calculate(sc);
	}

	public static void calculate(Scanner sc) {

		System.out.println("Podaj liczb� naturaln�...");
		long number = sc.nextLong();
		System.out.println("Suma cyfr : " + sumOfDigits(number));
		double evenAvg = avg(getEvenDigits(number));
		System.out.println("�rednia liczb parzystych = "+evenAvg);
		double oddAvg = avg(getOddDigits(number));
		System.out.println("�rednia liczb nieparzystych = "+oddAvg);

	}

	public static int sumOfDigits(long number) {

		int[] digits = getDigits(number);

		int sum = 0;
		for (int digit : digits) {
			sum += digit;
		}
		return sum;
	}

	public static int[] getOddDigits(long number) {

		int[] digits = getDigits(number);
		List<Integer> oddNumbers = new ArrayList<>();
		for (int digit : digits) {
			if (digit % 2 == 1) {
				oddNumbers.add(digit);
			}
		}
		return toArray(oddNumbers);
	}

	public static int[] getEvenDigits(long number) {

		int[] digits = getDigits(number);
		List<Integer> evenNumbers = new ArrayList<>();
		for (int digit : digits) {
			if (digit % 2 == 0) {
				evenNumbers.add(digit);
			}
		}
		return toArray(evenNumbers);
	}

	public static int[] getDigits(long number) {

		String parseNumbers = "" + number;
		String[] splitedDigits = parseNumbers.split("");
		int[] digits = new int[splitedDigits.length];

		for (int i = 0; i < splitedDigits.length; i++) {
			digits[i] = Integer.parseInt(splitedDigits[i]);
		}

		return digits;
	}

	public static int[] toArray(List<Integer> source) {
		int[] copy = new int[source.size()];

		for (int i = 0; i < source.size(); i++) {
			copy[i] = source.get(i);
		}
		return copy;
	}

	public static double avg(int[] arr) {
		return sum(arr) / (double) arr.length;
	}

	public static int sum(int[] arr) {
		int sum = 0;
		for (int num : arr) {
			sum += num;
		}
		return sum;
	}

}
