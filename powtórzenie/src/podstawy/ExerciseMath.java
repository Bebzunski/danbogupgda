package podstawy;

public class ExerciseMath {

	public static void main(String[] args) {
		System.out.println(getExtreme(1, 1, 2));
	}

	public static double getExtreme(double a, double b, double c) {
		double extreme = 0;
		try {
			double x0 = -b / (2 * a);
			extreme = a * x0 * x0 + b * x0 + c;
		} catch (ArithmeticException e) {
			System.out.println("Liczba a nie mo�e by� zerem");
		}
		return extreme;
	}

}
