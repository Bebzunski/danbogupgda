package podstawy;

public class Polynomial {

	private int[] coefficients;

	public Polynomial(int[] coefficients) {
		super();
		this.coefficients = coefficients;
	}

	public static void main(String[] args) {

		Polynomial pol = new Polynomial(new int[] { 0, -6, 0, -9, 4 });
		System.out.println(pol);
		System.out.println(pol.getDerivative());
	}

	public Polynomial() {
		super();
	}

	public void setCoefficient(int index, int value) {
		coefficients[index] = value;
	}

	@Override
	public String toString() {
		
		String wielomian = "";

		if (coefficients[0] != 0) {
			wielomian = getPlus(coefficients[0]) + coefficients[0];
		}
		if (coefficients[1] != 0) {
			wielomian = getPlus(coefficients[1]) + coefficients[1] + "x" + wielomian;
		}

		for (int i = 2; i < coefficients.length; i++) {
			if (coefficients[i] != 0) {
				wielomian = getPlus(coefficients[i]) + coefficients[i] + "x^" + i + wielomian;

			}
		}
		if (wielomian.charAt(0) == '+') {
			return wielomian.substring(1, wielomian.length());
		}

		return wielomian;
	}

	public String getPlus(int num) {
		if (num > 0) {
			return "+";
		} else {
			return "";
		}
	}

	public int[] getCoefficients() {
		return coefficients;
	}

	public void setCoefficients(int[] coefficients) {
		this.coefficients = coefficients;
	}

	public Polynomial getDerivative() {
		int[] der = new int[this.getCoefficients().length - 1];
		for (int i = 0; i < der.length; i++) {
			der[i] = this.getCoefficients()[i + 1] * (i + 1);
		}
		return new Polynomial(der);
	}
}
