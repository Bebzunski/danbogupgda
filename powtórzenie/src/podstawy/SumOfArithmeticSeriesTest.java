package podstawy;

public class SumOfArithmeticSeriesTest {

	public static void main(String[] args) {

		System.out.println(sumOfArithmeticSeries(11, 10));

	}

	public static int sumOfArithmeticSeries(int smaller, int bigger) {

		if (smaller > bigger) {
			throw new IllegalArgumentException(smaller + " nie jest mniejsze od " + bigger + " !");
		}
		int sum = 0;
		for (int value = smaller; value <= bigger; value++) {
			sum += value;
		}
		return sum;
	}

}
