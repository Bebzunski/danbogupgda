package rekurencja;

public class Excercise {
	
	public static void main(String[] args) {
		System.out.println(calculate(1, 1));
	}

	public static int calculate(int n, int m) {

		if (n == 0) {
			return m;
		} else if (m == 0) {
			return n;
		} else {
			return calculate(n - 1, m) + calculate(n, m - 1) + calculate(n - 1, m - 1);
		}
	}
}
