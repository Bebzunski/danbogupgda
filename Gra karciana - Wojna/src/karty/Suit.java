package karty;

public enum Suit {
	

	SPADES("s"), DIAMOND("d"), CLUB("c"), HEART("h");
	private String colour;

	private Suit(String suit) {
		this.colour = suit;
	}

	private Suit() {
	}

	public String getColour() {
		return colour;
	}

	public void setSuit(String suit) {
		this.colour = suit;
	}

}
