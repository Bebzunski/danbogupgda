package gra;

import stosy.Player;

public class GameContent {

	private Game game;
	private Player first;

	public GameContent(Game game) {
		this.game = game;
		first = game.getFirstPlayer();
	}

	public void printIntro() {
		MyLine.placeCenter("GRA W WOJN�" + "\n");
		MyLine.placeCenter(getFisrtPlayerName() + "   VS   " + game.getSecondPlayer().getPlayerName());
	}

	public Player getFistPlayer() {
		return first;
	}

	public void startingRoundContent(int nrOfWar) {
		System.out.println("------------------------------------------------------------------------");
		MyLine.placeCenter("RUNDA " + nrOfWar + ".");
	}
	
	public static String playersCardsContent(int n) {
		String cards = "";
		for (int i = 1; i <= n; i++) {
			cards += "[X]";
		}
		return cards;
	}
	
	public void warContent() {

		MyLine.placeLeft(getFisrtPlayerName());
		MyLine.placeLeft(playersCardsContent(getFistPlayer().getNumberOfCards()) + "\n");

		game.getPot().printPotContent();

		MyLine.placeRight(playersCardsContent(game.getSecondPlayer().getNumberOfCards()));
		MyLine.placeRight(game.getSecondPlayer().getPlayerName());
		System.out.println();

	}

	public void printCurrentResult() {

		MyLine.placeTwoCorners(
				getFisrtPlayerName() + ": " + getFistPlayer().getNumberOfCards() + " kart",
				game.getSecondPlayer().getPlayerName() + ": " + game.getSecondPlayer().getNumberOfCards() + " kart");
	}

	public String getFisrtPlayerName() {
		return getFistPlayer().getPlayerName();
	}

	public void printEndingGame() {

		System.out.println();
		System.out.println();
		if (game.getSecondPlayer().getNumberOfCards() == 0) {
			MyLine.placeCenter(getFisrtPlayerName() + " WYGRA� WOJN� !!");
		} else {
			MyLine.placeCenter(game.getSecondPlayer().getPlayerName() + " WYGRA� WOJN� !!");
		}
	}

}
