package gra;

import stosy.DeckOf24Cards;
import stosy.Player;
import stosy.Pot;

import java.util.Scanner;

public class Game {

	private Player firstPlayer = new Player();
	private Player secondPlayer = new Player();
	private Pot pot = new Pot();
//	private Scanner sc = new Scanner(System.in);
	private GameContent gc = new GameContent(this);
	
	public Game(){
		this("Gracz_A" , "Gracz_B");
	}

	public Game(String name1, String name2) {

		firstPlayer.setPlayerName(name1);
		secondPlayer.setPlayerName(name2);

		DeckOf24Cards deck24 = new DeckOf24Cards();
		deck24.takeAwayCards(firstPlayer, secondPlayer);
	}

	public void startGame() {

		gc.printIntro();
		int nrOfRound = 1;

		while (firstPlayer.getNumberOfCards() > 0 && secondPlayer.getNumberOfCards() > 0) {
//			sc.nextLine();
			warOfCards(nrOfRound);
			gc.printCurrentResult();
			nrOfRound++;
		}

		gc.printEndingGame();

	}

	public void warOfCards(int round) {

		gc.startingRoundContent(round);
		playersPutOneCard();
		gc.warContent();
//		sc.nextLine();
		while (whetherEqualCard()) {

			playWhenIsTie();
		}

		winingPlayerTakesPot();
	}

	public void playWhenIsTie() {

		MyLine.placeCenter("R E M I S !!");
		MyLine.placeCenter("(gracze k�ad� do puli po jednej karcie zakrytej oraz po jednej odkrytej)\n");
//		sc.nextLine();
		playersPutOneCard();
		playersPutOneCard();
		gc.warContent();
//		sc.nextLine();
	}

	private void winingPlayerTakesPot() {

		if (compereCards() > 0) {

			firstPlayer.takePot(pot.shuffle());
			MyLine.placeCenter(firstPlayer.getPlayerName() + " WYGRYWA ROZDANIE !");

		} else {

			secondPlayer.takePot(pot.shuffle());
			MyLine.placeCenter(secondPlayer.getPlayerName() + " WYGRYWA ROZDANIE !");
		}
	}

	private boolean whetherEqualCard() {
		return compereCards() == 0;
	}
	
	private int compereCards() {
		return Integer.compare(pot.get(pot.size() - 2).getValue().getId(),pot.get(pot.size() - 1).getValue().getId());
	}

	private void playersPutOneCard() {
		if (secondPlayer.getNumberOfCards() == 0) {
			firstPlayer.putCard(pot);
			firstPlayer.putCard(pot);
		} else if (firstPlayer.getNumberOfCards() == 0) {
			secondPlayer.putCard(pot);
			secondPlayer.putCard(pot);
		} else {
			firstPlayer.putCard(pot);
			secondPlayer.putCard(pot);
		}
	}

	public Player getFirstPlayer() {
		return firstPlayer;
	}

	public Player getSecondPlayer() {
		return secondPlayer;
	}

	public Pot getPot() {
		return pot;
	}

}
