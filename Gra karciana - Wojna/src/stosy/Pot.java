package stosy;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import gra.MyLine;
import karty.Card;

public class Pot {
	
	public List<Card> pot = new ArrayList<>();
	public String potContent = " vs ";

	public void printPotContent() {
		potContent = pot.get(0).printCard() + potContent + pot.get(1).printCard();
		for (int i = 2; i < pot.size(); i += 4) {
			potContent = " [X]" + potContent + "[X] ";
			potContent = pot.get(i + 2).printCard() + potContent + pot.get(i + 3).printCard();
		}
		
		MyLine.placeCenter(potContent);
		System.out.println();
		potContent = " vs ";
	}

	public int size() {
		return pot.size();
	}

	public Card get(int index) {
		return pot.get(index);
	}

	public Card remove() {
		Card c = pot.remove(0);
		return c;
	}
	
	public void add(Card c){
		pot.add(c);
	}
	
	public Pot shuffle(){
		Random r = new Random();
		List<Card> newPot = new ArrayList<>();
		int size = pot.size();

		for(int j = 0 ; j < size ; j++){
			int randomIndex = r.nextInt(pot.size());
			newPot.add(pot.remove(randomIndex));
		}
		pot = newPot;
		return this;
	}

}
