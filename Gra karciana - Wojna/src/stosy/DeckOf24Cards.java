package stosy;

import java.util.ArrayList;
//import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import karty.Card;

public class DeckOf24Cards {
	
	public List<Card> deckOf24Cards = new ArrayList<>();

	public DeckOf24Cards() {
		
		deckOf24Cards.addAll(Card.getAll());
		Collections.shuffle(deckOf24Cards);
		shuffle();
		
	}

	public void shuffle(){
		
		Random r = new Random();
		List<Card> deckOfCards = new ArrayList<>();
		int size = deckOf24Cards.size();

		for(int j = 0 ; j < size ; j++){
			int randomIndex = r.nextInt(deckOf24Cards.size());
			deckOfCards.add(deckOf24Cards.remove(randomIndex));
		}
		deckOf24Cards = deckOfCards;
	}
	
	public void takeAwayCards(Player p1 , Player p2){
		for(int i = 0 ; i < 12 ; i++){
			p1.takeCard(deckOf24Cards.remove(0));
			p2.takeCard(deckOf24Cards.remove(0));
		}
	}

}
