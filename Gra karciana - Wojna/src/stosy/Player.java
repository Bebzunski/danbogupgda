package stosy;

import java.util.ArrayList;
import java.util.List;

import karty.Card;

public class Player {
	

	public List<Card> deckOfCards = new ArrayList<>();
	private String playerName = "Gracz";
	
	public void putCard(Pot pot){
		pot.add(deckOfCards.remove(0));
	}
	
	public void takePot(Pot pot ){
		int size = pot.size();
		for(int i = 0 ; i < size ; i++){
			deckOfCards.add(pot.remove());
		}
	}
	
	public void takeCard(Card card){
		deckOfCards.add(card);
	}
	
	public int getNumberOfCards(){
		return deckOfCards.size();
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

}
