import java.util.ArrayList;
import java.util.List;

public class Sort {

    public static void main(String[] args) {

        int[] left = {3, 4, 5, 8 , 9 , 10};
        int[] right = {0, 4, 6, 7};
        int[] data = {3, 4, 5, 8, 9, 10, 0, 4, 6, 7};
        merge(left, right, data);

        for (int el : data) {
            System.out.print(el + " ");
        }
    }


    public static void sortowanieBabelkowe(int[] data) {

        int size = data.length - 1;
        int count;
        do {
            count = 0;
            for (int i = 0; i < size; i++) {
                int left = data[i];
                int right = data[i + 1];
                if (left > right) {
                    data[i] = right;
                    data[i + 1] = left;
                    count++;
                }
            }
            size--;
        } while (count > 0);
    }

    public static void sortowaniePrzezWstawianie(int[] arr) {
        int buffer;
        for (int i = 1; i < arr.length; i++) {
            buffer = arr[i];
            for (int j = i - 1; j >= 0; j--) {
                if (arr[j] > buffer) {
                    arr[j + 1] = arr[j];
                    if (j == 0) {
                        arr[j] = buffer;
                    }
                } else {
                    arr[j + 1] = buffer;
                    break;
                }
            }
        }
    }

    public static void mergeSort(int[] array) {
        int[] copy = new int[array.length];

        int middle = array.length / 2;


        for (int i = 0; i < array.length; i++) {
            copy[i] = array[i];
        }
    }

    public static void merge(int[] left, int[] right, int[] data) {
        int a = 0;
        int b = 0;
        for (int i = 0; i < (left.length + right.length) - 1; i++) {
            if (left[a] < right[b]) {
                data[a + b] = left[a];
                a++;
            } else {
                data[a + b] = right[b];
                b++;
            }
        }

        if (b == right.length) {
            data[a + b] = left[a];
        } else {
            data[a + b] = right[b];
        }
    }

}
