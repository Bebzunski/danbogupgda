import org.junit.Test;

import static org.junit.Assert.*;

public class SortTest {

    @Test
    public void bubleSortingTest() throws Exception {
        int[] actual = {3, 5, 1, 2, 8, -7, 4, 0, -3, 8};
        int[] expected = {-7, -3, 0, 1, 2, 3, 4, 5, 8, 8};
        Sort.sortowanieBabelkowe(actual);
        assertArrayEquals(expected , actual);
    }

    @Test
    public void sortByInsertionTest() throws Exception {
        int[] actual = {3, 5, 1, 2, 8, -7, 4, 0, -3, 8};
        int[] expected = {-7, -3, 0, 1, 2, 3, 4, 5, 8, 8};
        Sort.sortowaniePrzezWstawianie(actual);
        assertArrayEquals(expected , actual);
    }

    @Test
    public void mergeSortTesting() throws Exception {
        int[] actual = {3, 5, 1, 2, 8, -7, 4, 0, -3, 8};
        int[] expected = {-7, -3, 0, 1, 2, 3, 4, 5, 8, 8};
        Sort.mergeSort(actual);
        assertArrayEquals(expected , actual);
    }

}