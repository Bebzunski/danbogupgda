package kolekcje;

import java.util.Arrays;
import java.util.List;

public class CopingListToArray {
	
	public static void main(String[] args) {
		
		List<Integer> lista = Arrays.asList(4,6,8,1,33,5);
		int [] tablica = fromListToArray(lista);
		
		for(int n : tablica){
		System.out.print(n+" ");
		}
	}
	
	
	public static int[] fromListToArray(List<Integer> lista){
		
		int[] arr = new int[lista.size()];
		for(int i=0; i<lista.size();i++){
			arr[i]=lista.get(i);
		}
		
		return arr;
	}

}
