package kolekcje;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class ListMathTest {
	
	@Test
	public void sumTest(){
		
		List<Integer> lista = new ArrayList<Integer>();
		lista.add(8);
		lista.add(2);
		lista.add(1);
		
		assert ListMath.sum(lista) == 11;
	
	}
	
	@Test
	public void iloczynTest(){
		
		List<Integer> lista = new ArrayList<Integer>();
		lista.add(8);
		lista.add(2);
		lista.add(7);
		
		assert ListMath.iloczyn(lista) == 112;
	
	}
	
	@Test
	public void sredniaTest(){
		
		List<Integer> lista = new ArrayList<Integer>();
		lista.add(8);
		lista.add(2);
		lista.add(5);
		
		assert ListMath.srednia(lista) == 5;
	
	}

}
