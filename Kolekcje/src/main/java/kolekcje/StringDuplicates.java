package kolekcje;

import java.util.HashSet;
import java.util.Set;

public class StringDuplicates {

	public static boolean containDuplicates(String s){
		
		char[] charTable = s.toCharArray();
		Set<Character> set = new HashSet<>();
		
		for(char c : charTable){
			set.add(c);
		}
		
		return set.size()!=charTable.length;
		
	}
}
