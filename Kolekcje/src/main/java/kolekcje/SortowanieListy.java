package kolekcje;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import kolekcje.kolejki.Person;

public class SortowanieListy {

	public static void main(String[] args) {
		
		List<Person> persons = new ArrayList<>();
		persons.add(new Person("Pawe�",45));
		persons.add(new Person("Daniel",21));
		persons.add(new Person("Kasia",56));
		persons.add(new Person("jacek",34));
		persons.add(new Person("Anna",34));
		
		System.out.println("Przed sortowaniem");
		for(Person person:persons){
			System.out.println(person);
		}
		
		Collections.sort(persons);
		
		System.out.println("Po sortowaniem");
		for(Person person:persons){
			System.out.println(person);
		}
			
		Collections.shuffle(persons);
		
		System.out.println("Po tasowaniu");
		for(Person person:persons){
			System.out.println(person);
		}
	}
	

}
