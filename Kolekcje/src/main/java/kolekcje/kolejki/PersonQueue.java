package kolekcje.kolejki;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class PersonQueue {

	public static void main(String[] args) {
		
		Queue<Person> persons = new PriorityQueue<>(new Comparator<Person>() {

			@Override
			public int compare(Person o1, Person o2) {

				return o1.getName().compareTo(o2.getName());
			}
		});
		
		
		persons.add(new Person("Pawe�",45));
		persons.add(new Person("Daniel",21));
		persons.add(new Person("Kasia",56));
		persons.add(new Person("jacek",34));
		persons.add(new Person("Anna",34));
		
		Person p1 = new Person("Pawe�",45);
		System.out.println(p1.compareTo(new Person("Pawe�",45)));
		
		
		
		while(!persons.isEmpty()){
			
			System.out.println(persons.poll());
		}
		
		

		
	}

}
