package kolekcje;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListExamples {

	public static void main(String[] args) {
		
		List<Integer> lista = new ArrayList<Integer>();
		
		List<Integer> lista2 = new ArrayList<>(Arrays.asList(1,2,3));
		
		lista.remove(2);
		lista2.remove(2);
		
		System.out.println(lista.get(1));
		

		System.exit(0);
		
		List<Integer> numbers = new ArrayList<Integer>();
		numbers.add(2);
		numbers.add(3);
		numbers.add(5);
		numbers.add(7);
		
		System.out.println("Ca�a lista");
		for(int i : numbers){
			System.out.println(i);
		}
		
		System.out.println(numbers.get(2));
		numbers.remove(3);
		
		System.out.println("Ca�a lista");
		for(int i : numbers){
			System.out.println(i);
		}
		
		numbers.remove((Integer) 3);

		System.out.println("Ca�a lista");
		for(int i : numbers){
			System.out.println(i);
		}
		
		numbers.add(1, 4);
		
		System.out.println("Ca�a lista");
		for(int i : numbers){
			System.out.println(i);
		}
		
		System.out.println("Rozmiar : "+numbers.size());
	}

}
