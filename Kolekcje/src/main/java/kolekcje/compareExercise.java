package kolekcje;

import java.util.Arrays;
import java.util.List;

public class compareExercise {
	
	public static void main(String[] args) {
		
		int[] tablica = {1,2,3,4,5,6,7,8,9,0,8};
		List<Integer> lista = Arrays.asList(1,5,5,5,5,5,5,8,9);
		
		compareNumbers(lista, tablica);
	}
	
	public static void compareNumbers(List<Integer> lista, int[] tablica) {

		int count = 0;
		if (lista.size() == tablica.length) {
			for (int i = 0; i < lista.size(); i++) {
				if (lista.get(i) == tablica[i]) {
					count++;
				}
			}
			System.out.println(count);
		} else {
			System.out.println("Lista oraz tablica maj� r�ne d�ugo�ci.");
		}

	}

}
