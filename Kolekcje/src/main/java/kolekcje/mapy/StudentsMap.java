package kolekcje.mapy;

import java.util.Map;
import java.util.TreeMap;


public class StudentsMap {

	public static void main(String[] args) {
		
		Map<Integer,Student> students = new TreeMap<>();
		
		addStudent(students,100200, "Daniel", "Bogdalski");
		addStudent(students, 100400, "Kasia", "Kowalska");
		addStudent(students, 100700, "Anna", "Paj�k");
		addStudent(students, 200400, "Dariusz", "Kotlarczyk");
		addStudent(students, 200800, "Aneta", "W�odarczyk");
		System.out.println(students.size());
		students.remove(200400);
		System.out.println(students.size());
		
		System.out.println(students.containsKey(100900));
		
		showStudent(students,100400);
		System.out.println();
		for(Student stud : students.values()){
			System.out.println(stud.getImie()+" "+stud.getNazwisko());
		}

	}

	private static void showStudent(Map<Integer, Student> students , Integer numerIndeksu) {
		if(students.containsKey(numerIndeksu)==true){
		System.out.println(students.get(numerIndeksu).getImie()+" "+students.get(numerIndeksu).getNazwisko());
		}
		else{
			System.out.println("Nie ma studenta o indeksie "+numerIndeksu);
		}
	}

	private static void addStudent(Map<Integer, Student> students, Integer numerIndeksu , String imie , String nazwisko) {
		students.put(numerIndeksu, new Student(numerIndeksu,imie,nazwisko));
	}

}
