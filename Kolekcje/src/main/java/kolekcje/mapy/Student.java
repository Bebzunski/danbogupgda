package kolekcje.mapy;

public class Student {
	
	private Integer numerIndeksu;
	private String imie;
	private String nazwisko;
	
	public Student(Integer numerIndeksu, String imie, String nazwisko) {
		super();
		this.numerIndeksu = numerIndeksu;
		this.imie = imie;
		this.nazwisko = nazwisko;
	}

	public Integer getNumerIndeksu() {
		return numerIndeksu;
	}

	public String getImie() {
		return imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}
	

	

}
