package kolekcje.mapy;

import java.util.HashMap;
import java.util.Map;

public class MapsExample {

	public static void main(String[] args) {
	
		Map<String,String> slownik = new HashMap<>();
		slownik.put("Kot", "Cat");
		slownik.put("Pies", "Dog");
		
		System.out.println("Klucze : ");
		for(String value : slownik.values()){
			System.out.println(value);
		}
		
		slownik.remove("Pies");
		System.out.println("Pary klucz : warto�� : ");
		for(Map.Entry<String,String> pair : slownik.entrySet()){
			System.out.println(pair);
			
		System.out.println("Warto�� dla 'Kot' to : "+slownik.get("Kot"));
		}
		
		System.out.println(slownik.get("Kot"));
		System.out.println(slownik.getOrDefault("Pies","nie ma psa"));


	}

}
