package kolekcje.mapy;

import java.util.HashMap;
import java.util.Map;


public class University {
	
	private Map<Integer,Student> students = new HashMap<>();
	
	public void addStudent(int indexNumber, String name, String surname){
		Student student = new Student(indexNumber,name,surname);
		students.put(student.getNumerIndeksu(),student);
	}
	
	public boolean studentExists(int indexNumer){
		
		return students.containsKey(indexNumer);
	}
	
	public Student getStudent(int indexNumber){
		return students.get(indexNumber);
	}	
	public int studentsNumber(){
		return students.size();
	}
	
	public void showAll(){
		for(Student s : students.values()){
			System.out.println(s.getNumerIndeksu()+" "+ s.getImie()+" "+s.getNazwisko());
		}
	}

}
