package kolekcje;

import java.util.Arrays;
import java.util.List;

public class UserList {
	
	public static void main(String[] args) {
		
		List<User> users = Arrays.asList(new User("Adam","abcdds"),new User("Daniel","gadadsae"));
		
		for(User user : users){
			System.out.println(user.getName());
		}
	}

}
