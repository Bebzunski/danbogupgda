package workplace;

public class WorkplaceTest {
	
	public static void main(String[] args) {
		Workplace hotel = new Workplace();
		hotel.addWorker(55, "Daniel");
		hotel.addWorker(551, "Kasia");
		hotel.addWorker(34, "Ola");
		hotel.addWorker(1, "Jacek");
		hotel.addWorker(4, "Pawe�");
		
		hotel.printAllOccupants();
		hotel.printAllRooms();
	}
	
	
}
