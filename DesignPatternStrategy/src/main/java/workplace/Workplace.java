package workplace;

import java.util.HashMap;

public class Workplace {

	private HashMap<Integer, String> rooms = new HashMap<>();
	private HashMap<String, Integer> names = new HashMap<>();

	public void addWorker(int nr, String name) {
		rooms.put(nr, name);
		names.put(name, nr);
	}

	public String getOccupant(int nr) {
		return rooms.get(nr);
	}

	public void printAllOccupants() {
		for (Integer key : rooms.keySet()) {
			System.out.println("nr " + key + ": " + rooms.get(key));
		}
	}

	public void printAllRooms() {
		for (Integer key : rooms.keySet()) {
			System.out.println("pok�j nr " + key);
		}
	}
	
	public int findRoom(String name){
		return names.get(name);
	}

}
