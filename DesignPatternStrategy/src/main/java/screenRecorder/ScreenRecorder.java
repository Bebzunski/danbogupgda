package screenRecorder;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ScreenRecorder {

	private Profile currentProfile = null;
	private List<Profile> profiles = new ArrayList<>();
	private boolean isRecording = false;

	public void addProfile(Profile profile) {
		profiles.add(profile);
	}

	public void setProfile(int number) {
		if (number < 0 || number >= profiles.size()) {
			throw new IndexOutOfBoundsException("nie ma takiego profilu");
		}
		currentProfile = profiles.get(number);
	}

	public void listProfiles() {
		for (int i = 0; i < profiles.size(); i++) {
			System.out.println(i + " : " + profiles.get(i).getProfile());
		}
		System.out.println();
	}

	public void actualProfile() {
		if (getProfile()==null) {
			System.out.println("profil nie ustawiony");
		} else {
			System.out.println("aktualny profil: " + currentProfile.getProfile());
		}
	}

	public Profile getProfile() {
		return currentProfile;
	}

	public void addProfileFromConsole(Scanner sc) {

		Codec myCodec = getCodec(sc);
		Extension myExtension = getExtension(sc);
		Resolution myResolution = getResolution(sc);
		this.addProfile(new Profile(myCodec, myExtension, myResolution));

	}

	public void setProfileFromConsole(Scanner sc) {
		this.listProfiles();
		System.out.println("Wybierz numer:");
		this.setProfile(sc.nextInt());
	}

	public static Codec getCodec(Scanner sc) {

		System.out.println("Wybierz kodek:");
		System.out.println("1: " + Codec.h263);
		System.out.println("2: " + Codec.h264);
		System.out.println("3: " + Codec.theora);
		System.out.println("4: " + Codec.vr8);
		int index = sc.nextInt();
		if (index > 4 || index < 1) {
			return Codec.getFromValue(1);
		} else {
			return Codec.getFromValue(index);
		}
	}

	public static Extension getExtension(Scanner sc) {

		System.out.println("Wybierz format:");
		System.out.println("1: " + Extension.AVI);
		System.out.println("2: " + Extension.MKV);
		System.out.println("3: " + Extension.MP3);
		System.out.println("4: " + Extension.MP4);
		int index = sc.nextInt();

		return Extension.getFromValue(index);
	}

	public static Resolution getResolution(Scanner sc) {

		System.out.println("Wybierz rozdzielczo��:");
		int i = 1;
		for (Resolution element : Resolution.values()) {
			System.out.println((i++) + ": " + element.getResolution());
		}

		int index = sc.nextInt();

		return Resolution.getFromValue(index);
	}


	public void startFromConsole() {
		Scanner sc = new Scanner(System.in);
		String line;

		while (sc.hasNextLine()) {
			line = sc.next();
			if (line.trim().equals("add")) {

				this.addProfileFromConsole(sc);

			} else if (line.trim().equals("exit")) {

				break;

			} else if (line.trim().equals("list")) {

				this.listProfiles();

			} else if (line.trim().equalsIgnoreCase("start")) {

				if (!isRecording) {
					System.out.println("Nie wybra�es profilu ! Wybierz teraz !\n");
					this.setProfileFromConsole(sc);
					
				}
				isRecording = true;
				System.out.println("Program zacz�� nagrywa� !");

			} else if (line.trim().equalsIgnoreCase("setProfile")) {

				this.setProfileFromConsole(sc);

			} else if (line.trim().equalsIgnoreCase("stop")) {
				isRecording = false;
				System.out.println("Program przesta� nagrywa� !");
			} else if (line.trim().equalsIgnoreCase("getprofile")){
				actualProfile();
			}
		}
	}
}
