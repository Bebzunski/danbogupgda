package screenRecorder;

public enum Codec {
	 h263, h264, theora, vr8;

	public static Codec getFromValue(int value) {
		if (value == 1) {
			return Codec.h263;
		} else if (value == 2) {
			return Codec.h264;
		} else if (value == 3) {
			return Codec.h263;
		} else {
			return Codec.vr8;
		}
	}

}
