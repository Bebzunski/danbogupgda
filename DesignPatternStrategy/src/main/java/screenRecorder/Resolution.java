package screenRecorder;

public enum Resolution {
	
	r_1280x1024("1280x1024") , r_1920x1080("1920x1080") ,  r_2048x1152("2048x1152");
	
	private String resolution;

	private Resolution(String resolution) {
		this.resolution = resolution;
	}

	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}
	
	public static Resolution getFromValue(int value) {
		if (value == 1) {
			return Resolution.r_1280x1024;
		} else if (value == 2) {
			return Resolution.r_1920x1080;
		} else {
			return Resolution.r_2048x1152;
		}
	}
	
	
}
