package screenRecorder;

public enum Extension {
	MP4, MP3, MKV, AVI;

	public static Extension getFromValue(int value) {
		if (value == 1) {
			return Extension.AVI;
		} else if (value == 2) {
			return Extension.MKV;
		} else if (value == 3) {
			return Extension.MP3;
		} else {
			return Extension.MP4;
		}
	}
}
