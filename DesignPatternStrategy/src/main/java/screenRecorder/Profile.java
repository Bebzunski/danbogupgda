package screenRecorder;

public class Profile {
	
	private Codec codec;
	private Extension extension;
	private Resolution resolution;
	
	public Profile(Codec codec, Extension extension, Resolution resolution) {
		this.codec = codec;
		this.extension = extension;
		this.resolution = resolution;
	}

	public Codec getCodec() {
		return codec;
	}

	public Extension getExtension() {
		return extension;
	}

	public String getResolution() {
		return resolution.getResolution();
	}
	
	public String getProfile(){
		return codec.name()+", "+extension.name()+", "+getResolution();
	}
	
}
