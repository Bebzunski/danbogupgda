package strategy;

public interface IStrategy {
	
	void fight();

}
