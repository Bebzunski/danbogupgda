package adresURL;

import java.util.HashMap;
import java.util.Map;

public class MakeRequestURLTest {

	public static void main(String[] args) {
		
		String host = "http://test.pl/index.jsp";
		Map<String, String> params = new HashMap<>();
		params.put("klucz", "warto��");
		params.put("k3", "v3");
		
		System.out.println(makeRequestURL(host, params));
	}
	
	public static String makeRequestURL(String url, Map<String, String> params){
		url=url + "?";
		for(String key : params.keySet()){
			url = url + key + "="+params.get(key)+"&";
		}
		return url.substring(0, url.length()-1);
	}

}
