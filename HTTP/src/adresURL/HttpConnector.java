package adresURL;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class HttpConnector {

	// private URL URL;
	private HttpURLConnection con;
	private String userAgent;

	public String sendGET(String url) throws IOException {
		URL fromUser = new URL(url);
		con = (HttpURLConnection) fromUser.openConnection();
		userAgent = "Aneta/1.0";
		con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", userAgent);
		int responseCode = con.getResponseCode();
		String result = "";
		if (responseCode == 200) {
			// pobieranie danych poprzez streamowaniex
			InputStream res = con.getInputStream();
			InputStreamReader reader = new InputStreamReader(res);
			Scanner sc = new Scanner(reader);
			while (sc.hasNextLine()) {
				result += sc.nextLine();
			}
			sc.close();
			System.out.println(result);
		}
		return result;
	}

	public String sendPOST(String url, String params) {
		return url;
	}

}
