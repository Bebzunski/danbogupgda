package adresURL;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class HttpConnectorExample {

	public static void main(String[] args) throws IOException {

		URL url = new URL("http://wp.pl");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		String userAgent = "Pawel/1.0";

		connection.setRequestMethod("GET");
		connection.setRequestProperty("User-Agent", userAgent);

		int responseCode = connection.getResponseCode();
		String result = "";
		if (responseCode == 200) {
			InputStream res = connection.getInputStream();
			InputStreamReader reader = new InputStreamReader(res);
			Scanner sc = new Scanner(reader);

			while (sc.hasNextLine()) {
				result += sc.nextLine();
			}
			sc.close();
		}
		System.out.println(result);
		URL postURL = new URL("http://wp.pl");
		HttpURLConnection conPOST = (HttpURLConnection) postURL.openConnection();
		String postUA = "Pawe�/2.0";
		
		conPOST.setRequestMethod("POST");
		conPOST.setRequestProperty("User-Agent", postUA);
		
		String params = "login=admin2";
		conPOST.setDoOutput(true);
		
		DataOutputStream dos = new DataOutputStream(conPOST.getOutputStream());
		dos.writeBytes(params);
		dos.flush();
		dos.close();
		
		Scanner sc = new Scanner(new InputStreamReader(conPOST.getInputStream()));
		
	}
	
	

}
