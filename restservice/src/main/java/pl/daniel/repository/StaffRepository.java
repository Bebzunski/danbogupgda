package pl.daniel.repository;

import org.springframework.data.repository.CrudRepository;
import pl.daniel.entity.Staff;

public interface StaffRepository extends CrudRepository<Staff, Long> {

}
