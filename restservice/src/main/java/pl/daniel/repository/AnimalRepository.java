package pl.daniel.repository;

import org.springframework.data.repository.CrudRepository;
import pl.daniel.entity.Animal;

public interface AnimalRepository extends CrudRepository<Animal, Long>{
}
