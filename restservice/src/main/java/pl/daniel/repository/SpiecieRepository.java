package pl.daniel.repository;

import org.springframework.data.repository.CrudRepository;
import pl.daniel.entity.Spiecie;

public interface SpiecieRepository  extends CrudRepository<Spiecie , Long>{


}
