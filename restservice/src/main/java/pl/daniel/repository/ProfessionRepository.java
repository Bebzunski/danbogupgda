package pl.daniel.repository;

import org.springframework.data.repository.CrudRepository;
import pl.daniel.entity.Profession;

public interface ProfessionRepository extends CrudRepository<Profession, Long> {

}
