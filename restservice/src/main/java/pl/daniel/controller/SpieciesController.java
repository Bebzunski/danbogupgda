package pl.daniel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.daniel.repository.SpiecieRepository;
import pl.daniel.entity.Spiecie;
import java.util.List;


@CrossOrigin
@RestController
@RequestMapping("/spiecies")
public class SpieciesController {

    @Autowired
    private SpiecieRepository spiecieRepository;


    @RequestMapping("/add")
    public Spiecie add(@RequestParam(name = "name") String name,
                       @RequestParam(name = "description") String desc){
        Spiecie s = new Spiecie();
        s.setName(name);
        s.setDescription(desc);
        return spiecieRepository.save(s);
    }

    @RequestMapping("/show")
    public List<Spiecie> showAll(){
        return (List<Spiecie>) spiecieRepository.findAll();
    }

    @RequestMapping("/show/{id}")
    public Spiecie showById(@PathVariable(name = "id") String id) {
        long myId = Long.valueOf(id);
        return spiecieRepository.findOne(myId);
    }
}
