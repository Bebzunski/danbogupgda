package pl.daniel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.daniel.entity.Animal;
import pl.daniel.entity.Spiecie;
import pl.daniel.repository.AnimalRepository;
import pl.daniel.repository.SpiecieRepository;

import java.util.*;

@CrossOrigin
@RestController
@RequestMapping("/animals")
public class AnimalController {

    @Autowired
    private AnimalRepository animalRepository;

    @Autowired
    private SpiecieRepository spiecieRepository;

    @RequestMapping("/")
    public String animal() {
        return "";
    }

    @RequestMapping("/show")
    public List<Animal> listAnimals() {
        return (List<Animal>) animalRepository.findAll();
    }

    @RequestMapping("/add")
    public Animal addAnimal(@RequestParam(name = "name") String name,
                            @RequestParam(name = "description") String desc,
                            @RequestParam(name = "link") String link,
                            @RequestParam(name = "spiecie") String spiecie) {

        Long spiecieId = Long.valueOf(spiecie);
        Spiecie s = spiecieRepository.findOne(spiecieId);

        Animal a = new Animal();
        a.setName(name);
        a.setDescription(desc);
        a.setLink(link);

        a.setSpiecie(s);
        return animalRepository.save(a);
    }

    @RequestMapping("/show/{id}")
    public Animal showAnimalByID(@PathVariable("id") String id) {
        return animalRepository.findOne(Long.valueOf(id));
    }
}
