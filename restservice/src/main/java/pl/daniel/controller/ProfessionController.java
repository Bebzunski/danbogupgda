package pl.daniel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.daniel.entity.Profession;
import pl.daniel.repository.ProfessionRepository;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/professions")
public class ProfessionController {

    @Autowired
    private ProfessionRepository professionRepository;

    @RequestMapping("/add")
    public Profession add(@RequestParam(name = "name") String name ,
                          @RequestParam(name = "description") String description){
        Profession s = new Profession();
        s.setName(name);
        s.setDescription(description);
        return professionRepository.save(s);
    }

    @RequestMapping("/show")
    public List<Profession> showAll(){
        return (List<Profession>) professionRepository.findAll();
    }

    @RequestMapping("/show/{id}")
    public Profession showById(@PathVariable(name = "id") String id) {
        long myId = Long.valueOf(id);
        return professionRepository.findOne(myId);
    }
}
