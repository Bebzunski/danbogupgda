package pl.daniel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.daniel.entity.Profession;
import pl.daniel.entity.Staff;
import pl.daniel.repository.ProfessionRepository;
import pl.daniel.repository.StaffRepository;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/staffs")
public class StaffContoller {

    @Autowired
    private StaffRepository staffRepository;

    @Autowired
    private ProfessionRepository professionRepository;


    @RequestMapping("/")
    public String animal() {
        return "";
    }

    @RequestMapping("/show")
    public List<Staff> listStaffs() {
        return (List<Staff>) staffRepository.findAll();
    }

    @RequestMapping("/add")
    public Staff addStaff(@RequestParam(name = "name") String name,
                            @RequestParam(name = "lastname") String lastname,
                            @RequestParam(name = "salary") String salary,
                            @RequestParam(name = "profession") String profession) {

        Long professionId = Long.valueOf(profession);
        Profession s = professionRepository.findOne(professionId);

        Staff a = new Staff();
        a.setName(name);
        a.setLastname(lastname);
        a.setSalary(Double.valueOf(salary));

        a.setProfession(s);
        return staffRepository.save(a);
    }

    @RequestMapping("/show/{id}")
    public Staff showStaffByID(@PathVariable("id") String id) {
        return staffRepository.findOne(Long.valueOf(id));
    }

}
