package pl.daniel.entity;


import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

@Entity
public class Animal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String description;
    private String link;

    @JsonManagedReference
    @ManyToOne(cascade = CascadeType.ALL)
    private Spiecie spiecie;

    public Spiecie getSpiecie() {
        return spiecie;
    }

    public void setSpiecie(Spiecie spiecie) {
        this.spiecie = spiecie;
    }

    public Animal() {
    }

    public Animal(long id, String name, String description, String link) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.link = link;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}

