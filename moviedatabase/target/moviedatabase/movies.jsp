<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="header.jsp" />

<section>
    <link rel="stylesheet" href="style.css"/>
    <table class="myTable">
        <tr>

                <th>ID</th>
                <th>Nazwa</th>
                <th>Akcja</th>

        </tr>
        <tbody>
            <c:forEach items="${movies}" var="movie">
                <tr>
                    <td><c:out value="${movie.id}" /></td>
                    <td><c:out value="${movie.name}"/></td>
                    <td>Edytuj Usuń</td>
                </tr>
            </c:forEach>
        </tbody>

    </table>
</section>
<c:import url="footer.jsp" />
