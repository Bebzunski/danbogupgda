<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <title>Moja strona</title>
    <link rel="stylesheet" href="style.css"/>
</head>
<body>
<div id="container">
    <header>
        Nagłówek
        <ul>
            <li><a href="index.jsp" >Strona głowna</a></li>
            <li><a href="add.jsp" >Dodaj film</a></li>
            <li><a href="MovieServlet" >Przeglądaj film</a></li>
        </ul>
    </header>