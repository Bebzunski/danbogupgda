package pl.daniel.moviedatabase.servlet;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import pl.daniel.moviedatabase.entity.Movie;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "MovieServlet")
public class MovieServlet extends HttpServlet {

    SessionFactory sf = new Configuration().configure().buildSessionFactory();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if(action.equals("add")) {
            String name = request.getParameter("movieName");
            Session session = sf.openSession();
            Movie m = new Movie(name);
            Transaction t = session.beginTransaction();
            session.save(m);
            t.commit();
            session.close();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            Session session = sf.openSession();
            Transaction t = session.beginTransaction();
            List<Movie> movies = session.createQuery("from Movie").list();
            t.commit();
            session.close();

            request.setAttribute("movies", movies);
            request.getRequestDispatcher("movies.jsp").forward(request, response);
    }
}
