package pl.org.pfig.mytime;

public class MyTimeMain {

	public static void main(String[] args) {
		
		generateA();
		
		generateByBuilder();
		
//		int[] table1 = {1,5,7,4};
//		System.out.println(sum(table1));
//		int[][] table2 = new int[6][];
//		table2[2]= new int[] {2,3};
//		table2[3]= new int[] {5,3,6};
//		
//		for( int i : sumTable(table2)){
//			System.out.print(i+" ");
//		}
		
//		MyTime time1 = new MyTime(12,0,0);
//		System.out.println(time1);
//		MyTime time2 = time1.previousSecond();
//				System.out.println(time2);
	}

	private static void generateByBuilder() {
		StringBuilder builder = new StringBuilder();
		for(int i = 0 ; i<3000 ; i++){
			builder.append('a');
		}
	}

	private static void generateA() {
		String a = "";
		for(int i = 0 ; i<3000;i++){
			a+='a';
		}
		System.out.println(a);
	}
	
	public static int sum(int[] table){
		int suma = 0;
		for( int n : table){
			suma+=n;
		}
		
		return suma;
	}
	
	public static int[] sumTable(int[][] table) {
		int[] array = new int[table.length];
		for (int i = 0; i < table.length; i++) {
			if (table[i] != null) {
				array[i] = sum(table[i]);
			} else {
				array[i] = 0;
			}
		}
		return array;
	}

}
