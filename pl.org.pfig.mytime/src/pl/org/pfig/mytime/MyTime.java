package pl.org.pfig.mytime;

public class MyTime {
	
	private int hour = 0;
	private int minute = 0;
	private int second = 0;
	
	public MyTime() {
		super();
	}

	public MyTime(int hour, int minute, int second) {
		super();
		setTime(hour,minute,second);
	}
	
	public void setTime(int hour, int minute, int second){
	
	setHour(hour);
	setMinute(minute);
	setSecond(second);
	}
	
	public int getHour(){
		return hour;
	}

	public int getMinute() {
		return minute;
	}


	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		if(minute>=0 && minute<60){
		this.second = second;
		}
	}
	
	public void setHour(int hour){
		if(hour>=0 && hour<24){
		this.hour = hour;
		}
	}
	
	public void setMinute(int minute) {
		if(minute>=0 && minute<=59){
		this.minute = minute;
		}
	}
	
	public String leadZero(int num){
		if(num<10){
			return "0"+num;
		} else {
			return ""+num;
		}
	}
	
	public String toString(){
		return leadZero(hour)+" : "+leadZero(minute)+" : "+leadZero(second);
	}
	
	public MyTime nextHour(){
		int newHour = hour + 1;
		if(newHour==24){
			newHour=0;
		}
		return new MyTime(newHour, minute, second);
	}
	
	public MyTime nextMinute(){
		int newMinute = minute + 1;
		int newHour = hour;
		if(newMinute==60 && newHour==23){
			newMinute=0;
			newHour=0;
		} else if (newMinute==60 && newHour<23){
			newMinute=0;
			newHour++;
		}
		return new MyTime(newHour, newMinute, second);
	}
	
	public MyTime nextSecond(){
		int newMinute = minute;
		int newHour = hour;
		int newSecond = second + 1;
		
		if(newSecond>59){
			newSecond = 0;
			newMinute++;
		}
		if(newMinute>59){
			newMinute=0;
			newHour++;
		}
		if(newHour>23){
			newHour=0;
		}
		return new MyTime(newHour,newMinute,newSecond);
		}
		
	
	public MyTime previousHour(){
		int newHour = hour -1 ;
		if(newHour==-1){
			newHour=23;
		}
		return new MyTime(newHour, minute, second);
	}
	
	public MyTime previousMinute(){
		int newMinute = minute -1 ;
		int newHour = hour;
		if(newMinute==-1 && newHour==0){
			newMinute=59;
			newHour=23;
		} else if (newMinute==-1 && newHour>0){
			newMinute=59;
			newHour--;
		}
		return new MyTime(newHour, newMinute, second);
	}
	
	public MyTime previousSecond(){
		int newSecond = second - 1;
		int newMinute = minute;
		int newHour = hour;
		if(newSecond == -1 && newMinute==0 && newHour==0){
			newSecond=59;
			newMinute=59;
			newHour=23;
		} else if (newSecond == -1 && newMinute==0 && newHour>0){
			newSecond=59;
			newMinute=59;
			newHour--;
		} else if(newSecond == -1 && newMinute>0){
			newSecond = 59;
			newMinute--;
		}
		return new MyTime(newHour, newMinute, newSecond);
	}
}
